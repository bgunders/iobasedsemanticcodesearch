package equality;

public interface EqualityTest {
    public boolean isEqual(Object o1, Object o2);
}
