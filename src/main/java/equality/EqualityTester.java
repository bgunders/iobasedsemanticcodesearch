package equality;

import java.util.*;

public class EqualityTester {
    private Collection<EqualityTest> tests;

    public EqualityTester(){
        tests = new HashSet<>();
    }

    public EqualityTester(Collection tests){
        this.tests = new HashSet<>();
        this.tests.addAll(tests);
    }

    public void addTest(EqualityTest test){
        tests.add(test);
    }

    public void removeTest(EqualityTest test){
        tests.remove(test);
    }

    public Collection<EqualityTest> getTests(){
        return tests;
    }

    public boolean isEqual(Object o1, Object o2){
        if(o1 == null && o2 == null){
            return true;
        }
        if(o1 == null || o2 == null){
            return false;
        }

        for(EqualityTest test : tests){
            if(test.isEqual(o1, o2)){
                return true;
            }
        }
        return false;
    }
}
