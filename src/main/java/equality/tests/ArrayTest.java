package equality.tests;

import equality.EqualityTest;

import java.util.Arrays;

public class ArrayTest implements EqualityTest {

    @Override
    public boolean isEqual(Object o1, Object o2) {
        if (!(o1.getClass().isArray() && o2.getClass().isArray())){
            return false;
        }

        Object[] o1A = {o1};
        Object[] o2A = {o2};

        return Arrays.deepEquals(o1A, o2A);
    }
}
