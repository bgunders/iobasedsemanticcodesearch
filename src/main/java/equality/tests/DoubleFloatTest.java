package equality.tests;

import equality.EqualityTest;

public class DoubleFloatTest implements EqualityTest {
    @Override
    public boolean isEqual(Object o1, Object o2) {
        if((o1.getClass() == Double.class && o2.getClass() == Double.class) || (o1.getClass() == double.class && o2.getClass() == double.class)){
            double d1 = (double) o1;
            double d2 = (double) o2;

            if (Math.abs(d1 - d2) < 0.0001 * Math.abs(d1)){
                return true;
            }

            if(Double.isInfinite(d1) && Double.isInfinite(d2)){
                return true;
            }

            if(Double.isNaN(d1) && Double.isNaN(d2)){
                return true;
            }
        }
        if((o1.getClass() == Float.class && o2.getClass() == Float.class) || (o1.getClass() == float.class && o2.getClass() == float.class)){
            float d1 = (float) o1;
            float d2 = (float) o2;

            if (Math.abs(d1 - d2) < 0.0001 * Math.abs(d1)){
                return true;
            }

            if(Float.isInfinite(d1) && Float.isInfinite(d2)){
                return true;
            }

            if(Float.isNaN(d1) && Float.isNaN(d2)){
                return true;
            }
        }
        return false;
    }
}
