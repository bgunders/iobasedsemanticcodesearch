package equality.tests;

import equality.EqualityTest;

public class StringIgnoreCaseTest implements EqualityTest {

    @Override
    public boolean isEqual(Object o1, Object o2) {
        if(!(o1 instanceof String && o2 instanceof  String)){
            return false;
        }
        return ((String) o1).equalsIgnoreCase((String) o2);
    }
}
