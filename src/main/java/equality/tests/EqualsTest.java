package equality.tests;

import equality.EqualityTest;

public class EqualsTest implements EqualityTest {

    @Override
    public boolean isEqual(Object o1, Object o2) {
        return o1.equals(o2);
    }
}
