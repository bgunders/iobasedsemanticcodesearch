package parser;

import main.G;
import methods.ClassObjectPair;
import utils.Util;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class PrimitiveArrayParser implements Parser{

    public PrimitiveArrayParser(){

    }

    public List<ClassObjectPair> parse(String toParse){

        Tokenizer tokenizer = new Tokenizer();
        List<Object> values = new ArrayList<>();
        List<Token> tokens = tokenizer.toTokens(toParse, values);

        if(tokens.get(0).equals(Token.OPEN_BRACKET)){
            return parseArray(tokens, values);
        } else {
            return consumePrimitive(tokens, values.get(0));
        }
    }

    private List<ClassObjectPair> consumePrimitive(List<Token> tokens, Object value){
        List<ClassObjectPair> classObjectPairs = new ArrayList<>();

        if(tokens.get(0) == Token.CHAR_START){
            if(tokens.get(0) == Token.CHAR_START && tokens.get(1) == Token.CHAR && tokens.get(2) == Token.CHAR_END){
                char valChar =  (String.valueOf(value)).charAt(0);
                classObjectPairs.add(new ClassObjectPair(char.class, valChar));
                classObjectPairs.add(new ClassObjectPair(Character.class, Character.valueOf(valChar)));
            } else {
                System.out.println("Can't parse: " + tokens);
            }
            tokens.remove(2);
            tokens.remove(1);
            tokens.remove(0);
        } else if(tokens.get(0) != Token.CHAR_START){
            Token token = tokens.get(0);
            switch (token){
                case INTEGER:
                    long valLong = (long) value;
                    classObjectPairs.add(new ClassObjectPair(long.class, (long) valLong));
                    classObjectPairs.add(new ClassObjectPair(Long.class, Long.valueOf((long) valLong)));
                    classObjectPairs.add(new ClassObjectPair(float.class, (float) valLong));
                    classObjectPairs.add(new ClassObjectPair(Float.class, Float.valueOf((float) valLong)));
                    classObjectPairs.add(new ClassObjectPair(double.class, (double) valLong));
                    classObjectPairs.add(new ClassObjectPair(Double.class, Double.valueOf((double) valLong)));

                    if(Byte.MIN_VALUE <= valLong && valLong <= Byte.MAX_VALUE){
                        classObjectPairs.add(new ClassObjectPair(byte.class, (byte) valLong));
                        classObjectPairs.add(new ClassObjectPair(Byte.class, Byte.valueOf((byte) valLong)));
                    }
                    if(Short.MIN_VALUE <= valLong && valLong <= Short.MAX_VALUE){
                        classObjectPairs.add(new ClassObjectPair(short.class, (short) valLong));
                        classObjectPairs.add(new ClassObjectPair(Short.class, Short.valueOf((short) valLong)));
                    }
                    if(Integer.MIN_VALUE <= valLong && valLong <= Integer.MAX_VALUE){
                        classObjectPairs.add(new ClassObjectPair(int.class, (int) valLong));
                        classObjectPairs.add(new ClassObjectPair(Integer.class, Integer.valueOf((int) valLong)));
                        classObjectPairs.add(new ClassObjectPair(Object.class, Integer.valueOf((int) valLong)));
                    }
                    break;
                case FLOATING_POINT:
                    double valDouble = (double) value;
                    classObjectPairs.add(new ClassObjectPair(double.class, valDouble));
                    classObjectPairs.add(new ClassObjectPair(Double.class, Double.valueOf(valDouble)));


                    if(Math.abs(valDouble) <= Float.MAX_VALUE){;
                        classObjectPairs.add(new ClassObjectPair(float.class, (float) valDouble));
                        classObjectPairs.add(new ClassObjectPair(Float.class, Float.valueOf((float) valDouble)));
                    }

                    if(Double.isInfinite(valDouble)){
                        classObjectPairs.add(new ClassObjectPair(float.class, Float.NEGATIVE_INFINITY));
                        classObjectPairs.add(new ClassObjectPair(float.class, Float.POSITIVE_INFINITY));
                        classObjectPairs.add(new ClassObjectPair(Float.class, Float.valueOf(Float.NEGATIVE_INFINITY)));
                        classObjectPairs.add(new ClassObjectPair(Float.class, Float.valueOf(Float.POSITIVE_INFINITY)));
                    }

                    if(Double.isNaN(valDouble)){
                        classObjectPairs.add(new ClassObjectPair(float.class, Float.NaN));
                        classObjectPairs.add(new ClassObjectPair(Float.class, Float.valueOf(Float.NaN)));
                    }

                    break;
                case BOOLEAN:
                    boolean valBoolean = (boolean) value;
                    classObjectPairs.add(new ClassObjectPair(boolean.class, valBoolean));
                    classObjectPairs.add(new ClassObjectPair(Boolean.class, Boolean.valueOf(valBoolean)));
                    break;
                default:
                    System.out.println("Can't parse: " + token);
            }
            tokens.remove(0);
        }
        return classObjectPairs;
    }

    private List<ClassObjectPair> parseArray(List<Token> tokens, List<Object> values){
        int levelNum = -1;
        List<List<Integer>> levels = new ArrayList<>();

        Map<Class, List<Object>> classListMap = new HashMap<>();

        int numValues = 0;

        while(tokens.size() > 0){
            Token token = tokens.get(0);
            List<Integer> level;
            switch (token){
                case OPEN_BRACKET:
                    levelNum += 1;

                    if(levels.size() < levelNum + 1){
                         level = new ArrayList<>();
                        if(tokens.get(1) == Token.CLOSE_BRACKET){
                            level.add(0);
                        } else {
                            level.add(1);
                        }
                        levels.add(level);
                    } else {
                        level = levels.get(levelNum);
                        if(tokens.get(1) == Token.CLOSE_BRACKET){
                            level.add(0);
                        } else {
                            level.add(1);
                        }
                    }
                    tokens.remove(0);
                    break;
                case CLOSE_BRACKET:
                    levelNum -= 1;
                    tokens.remove(0);
                    break;
                case SEPARATOR:
                    level = levels.get(levelNum);
                    int amount = level.get(level.size() - 1);
                    level.set(level.size() - 1, amount + 1);
                    tokens.remove(0);
                    break;
                default:
                    numValues++;
                    List<ClassObjectPair> cops = consumePrimitive(tokens, values.get(0));
                    values.remove(0);

                    for (ClassObjectPair cop : cops){
                        Class c = cop.getC();
                        Object o = cop.getO();

                        List<Object> objects;
                        if(classListMap.containsKey(c)){
                            objects = classListMap.get(c);
                        } else {
                            objects = new ArrayList<>();
                            classListMap.put(c, objects);
                        }
                        objects.add(o);
                    }

            }

        }
        List<ClassObjectPair> ret = new ArrayList<>();

        Class[] classes =
                {
                        byte.class,
                        Byte.class,
                        short.class,
                        Short.class,
                        int.class,
                        Integer.class,
                        long.class,
                        Long.class,
                        float.class,
                        Float.class,
                        double.class,
                        Double.class,
                        boolean.class,
                        Boolean.class,
                        char.class,
                        Character.class
                };

        if(classListMap.keySet().isEmpty()){
            if(levels.get(0).get(0) == 0){
                for (Class c : classes){
                    for(int i = 0; i < G.MAX_DIM; i++){
                        int[] dim = new int[i + 1];

                        Object baseArray = Array.newInstance(c, dim);
                        ret.add(new ClassObjectPair(baseArray.getClass(), baseArray));
                    }
                }
                return ret;
            } else {
                for(Class c: classes){
                    classListMap.put(c, new ArrayList<>());
                }
            }

        }


        for(Class c : classListMap.keySet()){
            List<Object> objects = classListMap.get(c);
            if(objects.size() == numValues){
                int[] dim = new int[levels.size()];
                dim[0] = levels.get(0).get(0);
                Object baseArray = Array.newInstance(c, dim);

                int k = 0;

                List<Object> arrays;
                arrays = new ArrayList<>();
                arrays.add(baseArray);
                ret.add(new ClassObjectPair(baseArray.getClass(), baseArray));

                if(c.equals(double.class) || c.equals(boolean.class)){
                    Object[] baseArrayA = {baseArray};
                    try {
                        ret.add(new ClassObjectPair(Object.class, Util.deepCopy(baseArrayA)[0]));
                    } catch (IOException e) {
                    } catch (ClassNotFoundException e) {
                    }
                }


                List<Object> newArrays;

                for(int levelIndex = 0; levelIndex < levels.size(); levelIndex++){
                    newArrays = new ArrayList<>();

                    List<Integer> inLevel = levels.get(levelIndex);

                    k = 0;

                    for(int i = 0; i < inLevel.size(); i++){
                        for(int j = 0; j < inLevel.get(i); j++) {

                            if (levelIndex == levels.size() - 1) {
                                Object fe = arrays.get(i);
                                Object og = objects.get(k);
                                Array.set(fe, j, og);
                            } else {
                                dim = new int[levels.size() - levelIndex - 1];
                                dim[0] = levels.get(levelIndex + 1).get(k);
                                Object arr = Array.newInstance(c, dim);
                                Array.set(arrays.get(i), j, arr);
                                newArrays.add(arr);
                            }
                            k++;

                        }

                    }
                    arrays = newArrays;
                }
            }
        }

        return ret;
    }
}
