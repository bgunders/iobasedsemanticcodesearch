package parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Tokenizer {




    public List<Token> toTokens(String toToken, List<Object> values){

        toToken = toToken.replace("{", " { ").replace(",", " , ")
                .replace("}", " } ").replace("[", " [ ")
                .replace("]", " ] ").replace("'", " ' ")
                .replace("true", " true "). replace("false", " false ").replace("'   '", " ___ ").replace("' '", " ___ ").replace("'　'", " ---- ").replace(" ' 　 ' ", " ---- ");
        List<Token> tokens = new ArrayList<>();


        Scanner sc = new Scanner(toToken);
        //sc.useDelimiter("");

        int charState = 0;

        while(true){
            if(sc.hasNext("----")) {
                sc.next("----");
                values.add('　');
                tokens.add(Token.CHAR_START);
                tokens.add(Token.CHAR);
                tokens.add(Token.CHAR_END);
            } else if(sc.hasNext("___")){
                sc.next("___");
                values.add(' ');
                tokens.add(Token.CHAR_START);
                tokens.add(Token.CHAR);
                tokens.add(Token.CHAR_END);
            } else if(sc.hasNext(".") && charState == 1){
                values.add(sc.next("."));
                tokens.add(Token.CHAR);
                charState += 1;
            } else if(sc.hasNext("\\{")){
                sc.next("\\{");
                tokens.add(Token.OPEN_BRACKET);
            } else if(sc.hasNext("}")){
                sc.next("}");
                tokens.add(Token.CLOSE_BRACKET);
            } else if (sc.hasNext("\\[")){
                sc.next("\\[");
                tokens.add(Token.OPEN_BRACKET);
            } else if (sc.hasNext("]")){
                sc.next("]");
                tokens.add(Token.CLOSE_BRACKET);
            } else if (sc.hasNext(",")){
                sc.next(",");
                tokens.add(Token.SEPARATOR);
            } else if (sc.hasNext("\\s+")){
                sc.next("\\s+");
            }  else if (sc.hasNextLong()) {
                values.add(sc.nextLong());
                tokens.add(Token.INTEGER);
            }else if (sc.hasNextDouble()){
                    values.add(sc.nextDouble());
                    tokens.add(Token.FLOATING_POINT);
            } else if (sc.hasNext("'") && (charState == 0 || charState == 2)){
                sc.next("'");

                if(charState == 0){
                    tokens.add(Token.CHAR_START);
                } else {
                    tokens.add(Token.CHAR_END);
                }
                charState = (charState + 1) % 3;
            }  else if(sc.hasNext("t")){
                sc.next("t");
                values.add(true);
                tokens.add(Token.BOOLEAN);
            } else if(sc.hasNext("f")){
                sc.next("f");
                values.add(false);
                tokens.add(Token.BOOLEAN);
            } else if(sc.hasNextBoolean()) {
                values.add(sc.nextBoolean());
                tokens.add(Token.BOOLEAN);
            }
            else {
                if(sc.hasNext()){
                    String s = sc.next();
                    if(s.length() == 1){
                        values.add((s.charAt(0)));
                        tokens.add(Token.CHAR_START);
                        tokens.add(Token.CHAR);
                        tokens.add(Token.CHAR_END);
                    } else {
                        System.out.println("Couldn't parse: ");
                        while(sc.hasNextLine()){
                            System.out.println(sc.nextLine());
                        }
                        break;
                    }
                } else {
                    break;
                }
            }
        }


        return tokens;

    }

}
