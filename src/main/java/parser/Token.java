package parser;

public enum Token {
    OPEN_BRACKET,
    CLOSE_BRACKET,
    BOOLEAN,
    FLOATING_POINT,
    INTEGER,
    CHAR_START,
    CHAR_END,
    CHAR,
    SEPARATOR
}
