package parser;

import methods.ClassObjectPair;

import java.util.List;

public interface Parser {
    public List<ClassObjectPair> parse(String toParse);

}
