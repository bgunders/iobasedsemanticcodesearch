package inputoutputMatching;

import main.G;
import methods.AllMethodInputs;
import methods.ClassObjectPair;
import methods.CurrentValues;
import methods.MethodContainer;
import process.ProcessExecutor;
import utils.HashUtils;
import utils.Util;

import java.io.*;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Function;

/*
This class is responsible for type filtering and matching input output pairs to methods.
This is done using maps.
 */
public class InputOutputMatcher extends ProcessExecutor implements Serializable {
    private static final long serialVersionUID = 8237492874223435L;

    public Map<List<Class>, Map<List<Object>, Set<Integer>>> inputOutputMap;
    public Map<Integer, MethodContainer> methodMap;
    public Map<List<Class>, Set<Integer>> typeMap;



    /*
    Iterate over all methods and invoke them on the template inputs.

    Then put them into the inputOutput map which is first a type filter and then second maps the input / output pairs
    to the set of methods which generate such input output pairs.

    The methods in such sets are represented as integers.
    methodMap represents the mapping between methods and integers.
    The typeMap maps the input / output types to the set of methods which can have such an input / output type.
     */
    public InputOutputMatcher(List<MethodContainer> methods){
        inputOutputMap = new HashMap<>();
        methodMap = new HashMap<>();
        typeMap = new HashMap<>();

        String exec = "java -jar target/InvokeProcess.jar param";
        int port = 58499;

        startProcess(exec, port);


        for(int i = 0; i < methods.size(); i++){

            MethodContainer methodContainer = methods.get(i);

            System.out.print("" + i + " ; " + methodContainer + "\n");
            methodMap.put(i, methodContainer);

            boolean maybeDet = true;

            for(int j = 0; j < G.TEMPLATES.size(); j++){
                CurrentValues currentValues = G.TEMPLATES.get(j);

                Class declaringClass = methodContainer.getDeclaringClass();

                AllMethodInputs ami = currentValues.getPossibleMethodInputs(methodContainer.getParameterTypes());
                Iterator<Object[]> it = ami.iterator();

                int bufferSize = 1024;

                invokeAll(exec, i, methodContainer, j, currentValues, declaringClass, it, bufferSize);

            }
        }

        closeAll();

        Set<Integer> nonDet = new HashSet<>();

        for(int i = 0; i < methods.size(); i++){
            MethodContainer methodContainer = methods.get(i);
            if(!methodContainer.isProbablyDet()){
                nonDet.add(i);
            }
        }

        for (int i : nonDet){
            methodMap.remove(i);
        }

        Set<List<Class>> ioKeySet = inputOutputMap.keySet();
        for(List<Class> key1 : ioKeySet){
            Map<List<Object>, Set<Integer>> map = inputOutputMap.get(key1);

            Set<List<Object>> mapKeySet = map.keySet();

            for(List<Object> key2 : mapKeySet){
                Set<Integer> set = map.get(key2);
                set.removeAll(nonDet);
            }
        }

        Set<List<Class>> typeKeySet = typeMap.keySet();


        for(List<Class> key : typeKeySet){
            Set<Integer> set = typeMap.get(key);

            set.removeAll(nonDet);
        }

        System.out.println("total num methods = " + methods.size());
        System.out.println("non det methods = " + nonDet.size());

        System.out.println("probably det methods = " + (methods.size() - nonDet.size()));
    }

    private void invokeAll(String exec, int i, MethodContainer methodContainer, int j, CurrentValues currentValues, Class declaringClass, Iterator<Object[]> it, int bufferSize) {
        boolean maybeDet = true;

        List<Object[]> paramBuffer = new ArrayList<>();
        List<Object> objectBuffer = new ArrayList<>();

        while(it.hasNext() && maybeDet){
            maybeDet = methodContainer.isProbablyDet();

            Object[] parameters = it.next();

            if(Modifier.isStatic(methodContainer.getMethod().getModifiers())){
                paramBuffer.add(parameters);
                objectBuffer.add(null);

                if(checkBuffer(paramBuffer, methodContainer, objectBuffer, exec, bufferSize, i, j)){
                    paramBuffer = new ArrayList<>();
                    objectBuffer = new ArrayList<>();
                }


            } else {
                List<Object> declObjects = currentValues.getObjectsOfClass(declaringClass);
                if(declObjects == null){
                    declObjects = new ArrayList<>();
                }

                for (Object o: declObjects){
                    paramBuffer.add(parameters);
                    objectBuffer.add(o);

                    if(checkBuffer(paramBuffer, methodContainer, objectBuffer, exec, bufferSize, i, j)){
                        paramBuffer = new ArrayList<>();
                        objectBuffer = new ArrayList<>();
                    }
                }
            }
        }
        if(paramBuffer.size() > 0){
            if(checkBuffer(paramBuffer, methodContainer, objectBuffer, exec, 1, i, j)){
                paramBuffer = new ArrayList<>();
                objectBuffer = new ArrayList<>();
            }
        }
    }

    /*
    Check if paramBuffer is full enough. If yes then invoke the method in methodContainer using all the different
    parameter variations in paramBuffer and objectBuffer.
    After that all possible input output pairs are added to the inputOutputMap. Also the typeMap is populated.

    Method is invoked by calling "java -jar target/InvokeProcess.jar param" (exec). This is isolates the method invocation
    from the normal program and allows for forced termination (threads in java can't be forced to terminate if
    they aren't cooperative).
     */
    public boolean checkBuffer(List<Object[]> paramBuffer, MethodContainer methodContainer, List<Object> objectBuffer, String exec, int bufferSize, int i, int templateId){
        if(paramBuffer.size() >= bufferSize){
            if(!methodContainer.isProbablyDet()){
                return true;
            }
            List<Object[]> paramBufferCopy = null;
            List<Object> objectBufferCopy = null;
            try {
                 paramBufferCopy = (List<Object[]>) Util.deepCopy(paramBuffer);
                 objectBufferCopy = (List<Object>) Util.deepCopy(objectBuffer);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            List<List<ClassObjectPair>> out = invokeMethodExt(methodContainer, paramBuffer, objectBuffer, exec, methodContainer.getParameterTypes());
            restartProcess(exec);
            List<List<ClassObjectPair>> out2 = invokeMethodExt(methodContainer, paramBufferCopy, objectBufferCopy, exec, methodContainer.getParameterTypes());
            restartProcess(exec);
            if(out == null){
                return true;
            }

            if(out.size() != out2.size()){
                methodContainer.setProbablyDet(false);
                System.out.println("A");
                System.out.println("out");
                System.out.println(out);
                System.out.println("out2");
                System.out.println(out2);
                System.out.println(methodContainer);
                return true;
            }

            for(int j = 0; j < out.size(); j++){
                if(out.get(j).size()!= out2.get(j).size()){
                    methodContainer.setProbablyDet(false);
                    System.out.println("B");
                    System.out.println("out");
                    System.out.println(out);
                    System.out.println("out2");
                    System.out.println(out2);
                    System.out.println(methodContainer);
                    return true;
                }

                for(int k = 0; k < out.get(j).size(); k++){
                    if(out.get(j).get(k).getO() == null || out2.get(j).get(k).getO() == null){
                        continue;
                    }

                    if(!out.get(j).get(k).equals(out2.get(j).get(k))){
                        methodContainer.setProbablyDet(false);
                        System.out.println("C");
                        System.out.println("out");
                        System.out.println(out);
                        System.out.println("out2");
                        System.out.println(out2);
                        System.out.println(methodContainer);
                        return true;
                    }
                }
            }


            for (int j = 0; j < paramBuffer.size(); j++){
                for(ClassObjectPair cop : out.get(j)){
                    List<Class> inputOutputType = getInputOutputType(methodContainer.getParameterTypes(), cop.getC());
                    List<Object> inputOutputKey = getInputOutput(paramBuffer.get(j), cop.getO());

                    methodContainer.addInputOutput(inputOutputKey, inputOutputType, templateId);

                    if(!inputOutputMap.containsKey(inputOutputType)){
                        inputOutputMap.put(inputOutputType, new HashMap<>());
                    }

                    if(!typeMap.containsKey(inputOutputType)){
                        typeMap.put(inputOutputType, new HashSet<>());
                    }

                    typeMap.get(inputOutputType).add(i);

                    Map<List<Object>, Set<Integer>> inputOutputTypeMap = inputOutputMap.get(inputOutputType);

                    if(!inputOutputTypeMap.containsKey(inputOutputKey)){
                        inputOutputTypeMap.put(inputOutputKey, new HashSet<>());
                    }

                    Set<Integer> methodSet = inputOutputTypeMap.get(inputOutputKey);
                    methodSet.add(i);
                }
            }
            return true;
        }
        return false;
    }


    /*
    Invoke the method in methodContainer using all the possible parameters (Object[]) of paramsList.

    Method is invoked by calling "java -jar target/InvokeProcess.jar param" (exe). This is isolates the method invocation
    from the normal program and allows for forced termination (threads in java can't be forced to terminate if
    they aren't cooperative).

    This method communicates with InvokeProcess.java
     */
    private  List<List<ClassObjectPair>> invokeMethodExt(MethodContainer methodContainer, List<Object[]> paramsList, List<Object> objsList, String exec, Class[] types) {
        List<List<ClassObjectPair>> ret = new ArrayList<>();

        Object[][] dummy = new Object[1][1];

        Object[] objs = objsList.toArray();
        Object[][] params = paramsList.toArray(dummy);

        for(int i = 0; i < params.length; i++){
            ret.add(new ArrayList<>());
        }

        try {
            objectOutputStream.writeObject(params);
            objectOutputStream.writeObject(methodContainer.getJarFilePath());
            objectOutputStream.writeObject(methodContainer.getClassName());
            objectOutputStream.writeObject(methodContainer.getMethod().getName());
            objectOutputStream.writeObject(methodContainer.getParameterTypes());
            objectOutputStream.writeObject(objs);

            String found = (String) objectInputStream.readObject();

            if(found.equals("method not found")){
                System.out.println("method not found");
                return null;
            }


            Object[] returns = (Object[]) objectInputStream.readObject();
            Queue<Integer> done = (Queue<Integer>) objectInputStream.readObject();
            Queue<Integer> wontStop = (Queue<Integer>) objectInputStream.readObject();

            Object[][] retParams = (Object[][]) objectInputStream.readObject();
            Object[] retObjs = (Object[]) objectInputStream.readObject();

            for(int i : done){
                if(methodContainer.getReturnType() != void.class){
                    ret.get(i).add(new ClassObjectPair(methodContainer.getReturnType(), returns[i]));
                }

                for(int j = 0; j < types.length; j++){
                    if(!types[j].isPrimitive()){
                        if(retParams[i] != null){
                            ret.get(i).add(new ClassObjectPair(types[j], retParams[i][j]));
                        }
                    }
                }


                for(Object retO : retObjs){
                    if(retO != null){
                        ret.get(i).add(new ClassObjectPair(methodContainer.getDeclaringClass() , retO));
                    }
                }
            }


            if(wontStop != null){
                restartProcess(exec);

                List<Object[]> tempParams = new ArrayList<>();

                for(int i = 0; i < params.length; i++){
                    tempParams.add(params[i]);
                }

                for(int i : done){
                    tempParams.set(i, null);
                }

                for(int i : wontStop){
                    tempParams.set(i, null);
                    ret.get(i).add(new ClassObjectPair(methodContainer.getReturnType(), null));
                }

                List<List<ClassObjectPair>> tempRet = invokeMethodExt(methodContainer, tempParams, objsList, exec, types);

                for(int i = 0; i < tempRet.size(); i++){
                    if(!done.contains(i)){
                        ret.get(i).addAll(tempRet.get(i));
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return ret;
    }

    /*
    Return all integers which represent methods which can have that input output type.
     */
    public Set<Integer> getIndexesByType(Class[] inputTypes, Class outputTypes){
        List<Class> classIndex = getInputOutputType(inputTypes, outputTypes);
        return typeMap.get(classIndex);
    }

    /*
    Apply type filter and then get the set of integers which represent methods which have the
    listed input output pair.
     */
    public Set<Integer> getIndexes(Class[] inputTypes, Class outputTypes, Object[] inputs, Object output){
        List<Object> inputOutputIndex = getInputOutput(inputs, output);

        return getIndexes(inputTypes, outputTypes, inputOutputIndex);
    }

    public Set<Integer> getIndexes(Class[] inputTypes, Class outputTypes, List<Object> inputOutputIndex){
        List<Class> classIndex = getInputOutputType(inputTypes, outputTypes);

        Map<List<Object>, Set<Integer>> map = inputOutputMap.get(classIndex);

        if(map == null){
            return null;
        }

        Set<Integer> set = map.get(inputOutputIndex);
        return set;
    }

    /*
    Convert a set of integers to the set of methods said integers represent.
     */
    public Set<MethodContainer> getMethods(Set<Integer> ints){
        Set<MethodContainer> methodContainerSet = new HashSet<>();

        for (int i : ints){
            methodContainerSet.add(methodMap.get(i));
        }

        return methodContainerSet;
    }

    /*
    Convert input output type to a list which can be used for equality and hashing.
     */
    public static List<Class> getInputOutputType(Class[] input, Class output){
        List<Class> ret = new ArrayList<>();

        for(int i  = 0 ; i < input.length; i++){
            ret.add(input[i]);
        }
        ret.add(output);
        return ret;
    }

    /*
    Convert input output to a list which can be used for equality and hashing.
     */
    public static List<Object> getInputOutput(Object[] input, Object output){
        Object[] ret = new Object[input.length + 1];

        for(int i  = 0 ; i < input.length; i++){
            ret[i] = input[i];
        }
        ret[ret.length - 1] = output;
        return (List<Object>) HashUtils.toHashable(ret);
    }

    /*
    Write this instance to "inputOutputMatcher.txt"
     */
    public void serialize(){
        FileOutputStream fileOutputStream = null;
        try {
            //https://www.javatpoint.com/serialization-in-java

            fileOutputStream = new FileOutputStream("inputOutputMatcher.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileOutputStream != null){

            }
        }
    }


    /*
    Read an instance from "inputOutputMatcher.txt"
     */
    public static InputOutputMatcher deserialize(){
        InputOutputMatcher inputOutputMatcher = null;
        FileInputStream fileInputStream = null;
        try {
            //https://www.javatpoint.com/serialization-in-java

            fileInputStream = new FileInputStream("inputOutputMatcher.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            inputOutputMatcher = (InputOutputMatcher) objectInputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fileInputStream != null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return inputOutputMatcher;
    }

    public Function<Set<Integer>, Map<Set<List<Object>>, Set<Integer>>> getGroupFunction(){
        return (Set<Integer> set) -> {
            Map<Set<List<Object>>, Set<Integer>> ioPairsSet = new HashMap<>();

            for(int i : set){
                MethodContainer methodContainer = this.methodMap.get(i);
                Set<List<Object>> inputOutputSet = methodContainer.getInputOutputSet();
                List<Class> type = InputOutputMatcher.getInputOutputType(methodContainer.getParameterTypes(), methodContainer.getReturnType());

                List<Object> typeO = new ArrayList<>();

                for(Class c : type){
                    typeO.add(c);
                }

                Set<List<Object>> inputOutputSetCopy = new HashSet<>(inputOutputSet);

                inputOutputSetCopy.add(typeO);

                if(!ioPairsSet.containsKey(inputOutputSetCopy)){
                    ioPairsSet.put(inputOutputSetCopy, new HashSet<>());
                }

                ioPairsSet.get(inputOutputSetCopy).add(i);
            }

            return ioPairsSet;
        };
    }
}
