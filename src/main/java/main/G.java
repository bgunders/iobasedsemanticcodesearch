package main;

import equality.EqualityTester;
import equality.tests.ArrayTest;
import equality.tests.EqualsTest;
import equality.tests.StringIgnoreCaseTest;
import methods.ClassObjectPair;
import methods.CurrentValues;
import parser.PrimitiveArrayParser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class G {

    public static boolean CACHE = true;
    public static boolean TEMPLATE = true;

    public static int NUM_COPIES = 1;

    public static List<CurrentValues> TEMPLATES = new ArrayList<>();

    public static EqualityTester EQUALITY_TESTER = new EqualityTester();

    public static int ITERATIONS = 5;

    public static int MAX_DIM = 5;


    // EQUALITY_TESTER
    static{
        EQUALITY_TESTER.addTest(new ArrayTest());
        EQUALITY_TESTER.addTest(new EqualsTest());
        EQUALITY_TESTER.addTest(new StringIgnoreCaseTest());
    }

    // TEMPLATES
    static {
        PrimitiveArrayParser pap = new PrimitiveArrayParser();
        List<ClassObjectPair> ins;
        CurrentValues cv;

        ins = pap.parse("'0'");
        ins.addAll(pap.parse("'e'"));

        ins.addAll(pap.parse("[4,-1,4,2]"));
        ins.addAll(pap.parse("[1,32,1]"));

        ins.addAll(pap.parse("t"));
        ins.addAll(pap.parse("f"));

        ins.addAll(pap.parse("0"));
        ins.addAll(pap.parse("1"));

        ins.addAll(pap.parse("[[1,2],[3,4]]"));
        ins.addAll(pap.parse("[[-1,-2],[3,4,5]]"));

        ins.addAll(pap.parse("[[[5,2]]]"));
        ins.addAll(pap.parse("[[[1,2],[3,4]]]"));

        ins.addAll(pap.parse("['e', 'z']"));
        ins.addAll(pap.parse("['a','d']"));

        ins.addAll(pap.parse("[['a','b'],['c']]"));
        ins.addAll(pap.parse("[['4'],['¨', 'z'],['d']]"));

        ins.addAll(pap.parse("[[['a']]]"));
        ins.addAll(pap.parse("[[['b']]]"));

        ins.addAll(pap.parse("[t,f]"));
        ins.addAll(pap.parse("[t,f]"));

        ins.addAll(pap.parse("[[t,f]]"));
        ins.addAll(pap.parse("[[t,f],[f,t]]"));

        ins.addAll(pap.parse("[[[t]]]"));
        ins.addAll(pap.parse("[[[f]]]"));

        cv = new CurrentValues(ins);
        TEMPLATES.add(cv);

        ins = pap.parse("'A'");
        ins.addAll(pap.parse("'z'"));


        ins.addAll(pap.parse("[-10,-12]"));
        ins.addAll(pap.parse("[-1,-2,-3]"));

        ins.addAll(pap.parse("t"));
        ins.addAll(pap.parse("f"));

        ins.addAll(pap.parse("1"));
        ins.addAll(pap.parse("2"));

        ins.addAll(pap.parse("[[-10,3],[31,3]]"));
        ins.addAll(pap.parse("[[-1,-2,4],[3,2,-1],[-2,3,-1]]"));

        ins.addAll(pap.parse("[[[1]], [[2,12], [22,2]]]"));
        ins.addAll(pap.parse("[[[-1,-22],[6]]]"));

        ins.addAll(pap.parse("['+', 'ä']"));
        ins.addAll(pap.parse("['$','e', '4']"));

        ins.addAll(pap.parse("[['a','b','2'],['c','x','s'],['1','4','_']]"));
        ins.addAll(pap.parse("[['4'],['¨', 'z'],['d']]"));

        ins.addAll(pap.parse("[[['a','b','2'],['c','x','s'],['1','4','_']]]"));
        ins.addAll(pap.parse("[[['4'],['¨', 'z'],['d']]]"));

        ins.addAll(pap.parse("[t]"));
        ins.addAll(pap.parse("[f]"));

        ins.addAll(pap.parse("[[t,f],[]]"));
        ins.addAll(pap.parse("[[t,f,f,f],[f,t]]"));

        ins.addAll(pap.parse("[[[t],[f]],[[f]]]"));
        ins.addAll(pap.parse("[[[t],[f]],[[t]]]"));


        cv = new CurrentValues(ins);
        TEMPLATES.add(cv);

        ins = pap.parse("'p'");
        ins.addAll(pap.parse("'z'"));

        ins.addAll(pap.parse("[2,3,5]"));
        ins.addAll(pap.parse("[-1]"));

        ins.addAll(pap.parse("t"));
        ins.addAll(pap.parse("f"));

        ins.addAll(pap.parse("-1"));
        ins.addAll(pap.parse("1"));

        ins.addAll(pap.parse("[t,f]"));
        ins.addAll(pap.parse("[f,f,f]"));

        ins.addAll(pap.parse("[',']"));
        ins.addAll(pap.parse("['4','e', 'ä']"));

        ins.addAll(pap.parse("[['3','b','2'],['7','x','s'],['1','m','_']]"));
        ins.addAll(pap.parse("[['4'],['¨', 'z'],['d','d']]"));

        ins.addAll(pap.parse("[[['3','b','2'],['7','x','s']],[['1','m','_']]]"));
        ins.addAll(pap.parse("[[['4']],[['¨', 'z']],[['d'],['d']]]]"));

        ins.addAll(pap.parse("[[-12,4,2],[1,3,5]]"));
        ins.addAll(pap.parse("[[1,2],[-1,-1]]"));

        ins.addAll(pap.parse("[[[2]],[[23]]]"));
        ins.addAll(pap.parse("[[[1]]]"));

        ins.addAll(pap.parse("[[t,f],[t,t,t,t,t,f,f,f,f]]"));
        ins.addAll(pap.parse("[[t,f,f,t,t,f,f,f,f],[f,t,t,t,f,f]]"));

        ins.addAll(pap.parse("[[[t],[f]]]"));
        ins.addAll(pap.parse("[[[t]],[[f]]]"));

        cv = new CurrentValues(ins);
        TEMPLATES.add(cv);

        ins = pap.parse("'3'");
        ins.addAll(pap.parse("'U'"));

        ins.addAll(pap.parse("[4,5,6]"));
        ins.addAll(pap.parse("[-4,-5,-6]"));

        ins.addAll(pap.parse("[[1,2,3],[4,5,6]]"));
        ins.addAll(pap.parse("[[-1,-2,-3],[-4,-5,-6]]"));

        ins.addAll(pap.parse("[[[1,2,3],[4,5,6]]]"));
        ins.addAll(pap.parse("[[[-1,-2,-3],[-4,-5,-6]]]"));

        ins.addAll(pap.parse("t"));
        ins.addAll(pap.parse("f"));

        ins.addAll(pap.parse("42"));
        ins.addAll(pap.parse("420"));

        ins.addAll(pap.parse("[['3','b','2'],['7','x','s'],['1','m','_']]"));
        ins.addAll(pap.parse("[['4'],['¨', 'z'],['d','d']]"));

        ins.addAll(pap.parse("[[['3','b','2'],['7','x','s'],['1','m','_']]]"));
        ins.addAll(pap.parse("[[['4'],['¨', 'z'],['d','d']]]"));

        ins.addAll(pap.parse("['3','b','2']"));
        ins.addAll(pap.parse("['d','d']"));

        ins.addAll(pap.parse("[[t,f],[t,f,f,f,f]]"));
        ins.addAll(pap.parse("[[t,f,f],[f,t,t,t,f,f]]"));

        ins.addAll(pap.parse("[[[t],[f]],[[f,f,t]]]"));
        ins.addAll(pap.parse("[[[t],[t,f,f]],[[f]]]"));

        ins.addAll(pap.parse("[t,f]"));
        ins.addAll(pap.parse("[f,t,t,t,f,f]"));


        cv = new CurrentValues(ins);
        TEMPLATES.add(cv);

        /*

        ins = pap.parse("'3'");
        ins.addAll(pap.parse("[[1,2,3],[4,5,6]]"));
        ins.addAll(pap.parse("t"));
        ins.addAll(pap.parse("-32"));
        ins.addAll(pap.parse("2"));
        ins.addAll(pap.parse("'U'"));
        ins.addAll(pap.parse("[[-1,-2,-3],[-4,-5,-6]]"));
        ins.addAll(pap.parse("f"));
        cv = new CurrentValues(ins);
        TEMPLATES.add(cv);
         */
    }

    public static String templates =
            "Template 0\n \'0\',\'e\', [4,-1,4,2], [1,32,1],t,f,0,1, [[1,2],[3,4]], [[-1,-2],[3,4,5]],[[[5,2]]],[[[1,2],[3,4]]],[\'e\', \'z\'],[\'a\',\'d\'], [[\'a\',\'b\'],[\'c\']], [[\'4\'],[\'¨\', \'z\'],[\'d\']], [[[\'a\']]], [[[\'b\']]],[t,f],[t,f], [[t,f]], [[t,f],[f,t]], [[[t]]], [[[f]]]"
                    + "\nTemplate 1\n\'A\',\'z\', [-10,-12], [-1,-2,-3],t,f,1,2, [[-10,3],[31,3]], [[-1,-2,4],[3,2,-1],[-2,3,-1]], [[[1]], [[2,12], [22,2]]], [[[-1,-22],[6]]], [\'+\', \'ä\'], [\'\\$\',\'e\', \'4\'], [[\'a\',\'b\',\'2\'],[\'c\',\'x\',\'s\'],[\'1\',\'4\',\'\\_\']], [[\'4\'],[\'¨\', \'z\'],[\'d\']], [[[\'a\',\'b\',\'2\'],[\'c\',\'x\',\'s\'],[\'1\',\'4\',\'\\_\']]], [[[\'4\'],[\'¨\', \'z\'],[\'d\']]], [t], [f], [[t,f],[]], [[t,f,f,f],[f,t]], [[[t],[f]],[[f]]], [[[t],[f]],[[t]]]"
                    + "\nTemplate 2\n\'p\',\'z\', [2,3,5], [-1],t,f, -1,1,[t,f],[f,f,f], [\',\'],[\'4\',\'e\', \'ä\'], [[\'3\',\'b\',\'2\'],[\'7\',\'x\',\'s\'],[\'1\',\'m\',\'\\_\']], [[\'4\'],[\'¨\', \'z\'],[\'d\',\'d\']], [[[\'3\',\'b\',\'2\'],[\'7\',\'x\',\'s\']],[[\'1\',\'m\',\'\\_\']]], [[[\'4\']],[[\'¨\', \'z\']],[[\'d\'],[\'d\']]]], [[-12,4,2],[1,3,5]], [[1,2],[-1,-1]], [[[2]],[[23]]], [[[1]]], [[t,f],[t,t,t,t,t,f,f,f,f]], [[t,f,f,t,t,f,f,f,f],[f,t,t,t,f,f]], [[[t],[f]]], [[[t]],[[f]]]"
                    + "\nTemplate 3\n\'3\',\'U\', [4,5,6], [-4,-5,-6], [[1,2,3],[4,5,6]], [[-1,-2,-3],[-4,-5,-6]], [[[1,2,3],[4,5,6]]], [[[-1,-2,-3],[-4,-5,-6]]],t,f,42,420, [[\'3\',\'b\',\'2\'],[\'7\',\'x\',\'s\'],[\'1\',\'m\',\'\\_\']],[[\'4\'],[\'¨\', \'z\'],[\'d\',\'d\']], [[[\'3\',\'b\',\'2\'],[\'7\',\'x\',\'s\'],[\'1\',\'m\',\'\\_\']]],[[[\'4\'],[\'¨\', \'z\'],[\'d\',\'d\']]], [\'3\',\'b\',\'2\'], [\'d\',\'d\'], [[t,f],[t,f,f,f,f]], [[t,f,f],[f,t,t,t,f,f]], [[[t],[f]],[[f,f,t]]], [[[t],[t,f,f]],[[f]]],[t,f],[f,t,t,t,f,f]";


}
