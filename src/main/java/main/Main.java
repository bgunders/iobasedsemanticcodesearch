package main;

import com.sun.org.apache.bcel.internal.classfile.Utility;
import composition.MethodComposition;
import inputoutputMatching.InputOutputMatcher;
import methods.*;
import userinput.UserMethodMatcher;

import java.util.*;


public class Main {
    public static void main(String[] args) {

        System.out.println("Loading pre-computed data.");
        InputOutputMatcher inputOutputMatcher = InputOutputMatcher.deserialize();
        Set<Integer> methods;
        if(inputOutputMatcher != null){
            System.out.println("Data has been loaded.");

            Map<Integer, MethodContainer> methodMap = inputOutputMatcher.methodMap;
            methods = new HashSet<>(methodMap.keySet());
        } else {
            System.out.println("No data found.");
            methods = new HashSet<>();
            inputOutputMatcher = new InputOutputMatcher(new ArrayList<>());
        }

        UserMethodMatcher userMethodMatcher = new UserMethodMatcher(inputOutputMatcher, methods);
        //UserMethodMatcher userMethodMatcher = new UserMethodMatcher(null, null);
        userMethodMatcher.start();


        /*
        long start = System.currentTimeMillis();
        InputOutputMatcher inputOutputMatcher = InputOutputMatcher.deserialize();
        Map<Integer, MethodContainer> methods = inputOutputMatcher.methodMap;


        Map<Integer, MethodContainer> j8 = new HashMap<>();

        for(int mcInt : methods.keySet()){
            MethodContainer methodContainer = inputOutputMatcher.methodMap.get(mcInt);


            if(methodContainer.getJarFilePath().equals("")){
                j8.put(mcInt, methodContainer);
                methodContainer.loadMethod();
            }

        }

        MethodComposition methodComposition = new MethodComposition(j8, inputOutputMatcher);
        methodComposition.serialize();
         */


        /*
        int in = 42;
        double find = Math.cos(Math.sin(Math.tan(42)));

        List<Integer> templates = new ArrayList<>();
        templates.add(3);

        Set<ClassObjectPair> inputs = new HashSet<>();
        inputs.add(new ClassObjectPair(int.class, in));

        MethodComposition methodComposition = MethodComposition.deserialize();
        List<List<MethodContainer>> ret = methodComposition.findComposition(find, double.class, Integer.MAX_VALUE, templates);

        for(List<MethodContainer> r : ret){
            System.out.println("--");
            System.out.println(r);
        }


         */
        System.exit(0);
    }


}
