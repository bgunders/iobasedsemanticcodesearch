package filters;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.function.Predicate;

public class NumericMethodPredicate implements Predicate<Method> {
    @Override
    public boolean test(Method method) {
        int mod = method.getModifiers();

        if(true){

            Class ret = method.getReturnType();
            Class[] params = method.getParameterTypes();

            if (params.length == 0)
                return false;

            if(!isAcceptedClass(method.getDeclaringClass()) && !Modifier.isStatic(mod)){
                return false;
            }

            if(!Modifier.isStatic(mod)){
                return false;
            }



            /*
            Return type is void and all parameter types being of an accepted type and at least one is non primitive is ok.
             */
            if(ret == void.class){
                boolean atLeastOneNonPrimitive = false;

                for(Class param : params){
                    if(!param.isPrimitive()){
                        atLeastOneNonPrimitive = true;
                        break;
                    }
                }

                if(!atLeastOneNonPrimitive){
                    return false;
                }
            } else if(!isAcceptedClass(ret))
                return false;


            for(Class param : params){
                if(!isAcceptedClass(param))
                    return false;
            }

        } else {
            return false;
        }
        return true;
    }

    public static boolean isAcceptedClass(Class c) {
        while (c.isArray()) {
            c = c.getComponentType();
        }

        Class[] classes =
                {
                        byte.class,
                        Byte.class,
                        short.class,
                        Short.class,
                        int.class,
                        Integer.class,
                        long.class,
                        Long.class,
                        float.class,
                        Float.class,
                        double.class,
                        Double.class,
                        boolean.class,
                        Boolean.class,
                        char.class,
                        Character.class
                        //Object.class
                };


        for (Class cla : classes) {
            if (cla == c) {
                return true;
            }
        }
        return false;
    }
}
