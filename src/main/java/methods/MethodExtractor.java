package methods;

import compilation.JavaSrcDir;
import main.G;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.jar.JarFile;

public class MethodExtractor {

    Predicate<Method> acceptedTypes;


    public MethodExtractor(Predicate<Method> acceptedTypes){
        this.acceptedTypes = acceptedTypes;
    }

    public void addMethodsFromJar(File jar, List<MethodContainer> methods){
        //https://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar
        //https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/deployment/jar/examples/JarClassLoader.java
        //https://kostenko.org/blog/2019/06/runtime-class-loading.html

        URL[] urls = new URL[1];
        try {
            urls[0] = jar.toURI().toURL();
        } catch (Exception e) {
            e.printStackTrace();
        }

        URLClassLoader uc = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());

        try {
            JarFile jarFile = new JarFile(jar.getPath());
            jarFile.stream().forEach(
                    zipEntry -> {
                        if (zipEntry.isDirectory() || !zipEntry.getName().endsWith(".class")){
                            return;
                        }
                        // -6 because of .class

                        String className = zipEntry.getName().substring(0,zipEntry.getName().length()-6).replace('/', '.');
                        try {
                            Class c = Class.forName(className, false, uc);
                            Arrays.stream(c.getMethods()).filter(acceptedTypes).forEach(method -> {
                                for(int i = 0; i < G.NUM_COPIES; i++){
                                    if(method.getDeclaringClass() == c){
                                        methods.add(new MethodContainer(method, jar.getPath(), className));
                                    }
                                }
                            });
                            /*
                            for(Class declC : c.getDeclaredClasses()){
                                Arrays.stream(declC.getMethods()).filter(acceptedTypes).forEach(method -> {
                                    for (int i = 0; i < G.NUM_COPIES; i++){
                                        if(method.getDeclaringClass() == c) {
                                            methods.add(new MethodContainer(method, "", declC.getName()));
                                        }
                                    }
                                });
                            }
                             */
                        } catch (Exception e) {
                            return;
                        } catch (Error e){
                            return;
                        }
                    }
            );

        } catch (Exception e) {
        } catch (Error e){

        }
    }

    public void addJarDir(File jarDir, List<MethodContainer> methods){
        int begin = methods.size();

        int i = 0;

        if(jarDir.isDirectory()){

            for(File file : jarDir.listFiles()){
                i++;
                String path = file.getPath();
                if(path.endsWith(".jar")){
                    addMethodsFromJar(file, methods);
                }
            }
        }

        System.out.println("Extracted " + (methods.size() - begin) + " methods of " + jarDir);
    }

    public void addStdLibMethods(List<MethodContainer> methods){
        int begin = methods.size();

        JavaSrcDir jsd = new JavaSrcDir("java8src");
        List<String> classNames =  jsd.getClassNames();


        for(String className : classNames){

            try {
                Class<?> c = Class.forName(className);

                Arrays.stream(c.getMethods()).filter(acceptedTypes).forEach(method -> {
                    for (int i = 0; i < G.NUM_COPIES; i++){
                        if(method.getDeclaringClass() == c) {
                            methods.add(new MethodContainer(method, "", className));
                        }
                    }
                });
                /*
                for(Class declC : c.getDeclaredClasses()){
                    Arrays.stream(declC.getMethods()).filter(acceptedTypes).forEach(method -> {
                        for (int i = 0; i < G.NUM_COPIES; i++){
                            if(method.getDeclaringClass() == declC) {
                                //methods.add(new MethodContainer(method, "", declC.getName()));
                            }
                        }
                    });
                }
                 */

            } catch (Exception e) {
                //System.out.println(e);
            } catch (Error e){
                //System.out.println(e);
            }

        }



        System.out.println("Extracted " + (methods.size() - begin) + " methods of java8src");
    }

    public void getAllMethod(List<MethodContainer> methods){
        addStdLibMethods(methods);
        File jars = new File("jars/");
        addJarDir(jars, methods);
    }

}
