package methods;

import java.util.Iterator;
import java.util.List;

public class AllMethodInputs implements Iterable<Object[]> {
    Class[] classes;
    int[] max;
    CurrentValues values;


    public AllMethodInputs(Class[] classes, CurrentValues values){
        this.classes = classes;
        this.values = values;
        max = new int[classes.length];

        for(int i = 0 ;i < classes.length ; i++){
            List<Object> objs = values.getObjectsOfClass(classes[i]);

            if(objs != null){
                max[i] = values.getObjectsOfClass(classes[i]).size();
            } else {
                max[i] = 0;
            }


        }
    }

    @Override
    public Iterator<Object[]> iterator() {
        return new MethodInputIterator(classes, max, values);
    }

}
