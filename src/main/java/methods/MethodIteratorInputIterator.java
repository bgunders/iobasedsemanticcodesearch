package methods;

import java.util.Iterator;

public class MethodIteratorInputIterator implements Iterator<Object[]> {
    Class[] classes;
    int[] max;
    int[] index;
    CurrentValues values;

    Class newValueClass;
    Object newValue;
    int lowestClassIndex;

    boolean hasNext = true;

    public MethodIteratorInputIterator(Class[] classes, int[] max, CurrentValues values, Class newValueClass, Object newValue) {
        this.classes = classes;
        this.max = max;
        index = new int[classes.length];
        this.values = values;

        this.newValueClass = newValueClass;
        this.newValue = newValue;

        lowestClassIndex = 0;

        boolean first = true;

        for(int i = 0; i < max.length; i++){
            if(classes[i] == newValueClass){
                if(first){
                    lowestClassIndex = i;
                    first = false;
                }
                max[i]++;
            }
        }


        for(int i = 0; i < max.length; i++){
            if (max[i] == 0){
                hasNext = false;
                break;
            }
        }


        boolean atLeastOneNew = false;

        for(int i = 0; i < max.length; i++){
            if(classes[i] == newValueClass){
                if(index[i] == max[i] - 1){
                    atLeastOneNew = true;
                    break;
                }
            }
        }

        if(!atLeastOneNew){
            increment();
        }
    }

    private void increment(){
        boolean carry = true;

        for(int i = 0; i < classes.length; i++){
            if(index[i] < max[i] - 1){
                index[i] += 1;
                carry = false;
                break;
            } else {
                index[i] = 0;
            }
        }

        if(carry){
            hasNext = false;
            return;
        }

        boolean atLeastOneNew = false;

        for(int i = 0; i < max.length; i++){
            if(classes[i] == newValueClass){
                if(index[i] == max[i] - 1){
                    atLeastOneNew = true;
                    break;
                }
            }
        }

        if(!atLeastOneNew){
            increment();
        }
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public Object[] next() {
        Object[] objects = values.getValues(index, classes, newValue);
        increment();
        return objects;
    }
}
