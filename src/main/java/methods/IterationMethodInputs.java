package methods;

import java.util.Iterator;
import java.util.List;

public class IterationMethodInputs implements Iterable<Object[]>{


    Class[] classes;
    int[] max;
    CurrentValues values;

    Class newValueClass;
    Object newValue;


    public IterationMethodInputs(Class[] classes, CurrentValues values, Class newValueClass, Object newValue){
        this.classes = classes;
        this.values = values;

        this.newValueClass = newValueClass;
        this.newValue = newValue;

        max = new int[classes.length];

        for(int i = 0 ;i < classes.length ; i++){
            List<Object> objs = values.getObjectsOfClass(classes[i]);

            if(objs != null){
                max[i] = values.getObjectsOfClass(classes[i]).size();
            } else {
                max[i] = 0;
            }
        }
    }

    @Override
    public Iterator<Object[]> iterator() {
        return new MethodIteratorInputIterator(classes, max, values, newValueClass, newValue);
    }
}
