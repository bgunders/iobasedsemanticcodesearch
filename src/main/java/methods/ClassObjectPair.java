package methods;

import main.G;
import utils.HashUtils;
import utils.Util;
import java.io.Serializable;
import java.util.Objects;

public class ClassObjectPair implements Serializable {
    @Override
    public boolean equals(Object o1) {
        if (this == o1) return true;
        if (o1 == null || getClass() != o1.getClass()) return false;
        ClassObjectPair that = (ClassObjectPair) o1;
        return Objects.equals(c, that.c) && G.EQUALITY_TESTER.isEqual(o, that.o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(c, HashUtils.toHashable(o));
    }

    Class c;
    Object o;

    public ClassObjectPair(Class c, Object o){
        this.c = c;
        this.o = o;
    }

    public Class getC() {
        return c;
    }

    public Object getO() {
        return o;
    }

    @Override
    public String toString() {
        return "COP{" +
                "c=" + c +
                ", o=" + Util.primitiveArrayToString(o) +
                '}';
    }
}
