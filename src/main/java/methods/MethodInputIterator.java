package methods;

import java.util.Iterator;

public class MethodInputIterator implements Iterator<Object[]> {
    Class[] classes;
    int[] max;
    int[] index;
    CurrentValues values;

    boolean hasNext = true;

    public MethodInputIterator(Class[] classes, int[] max, CurrentValues values){
        this.classes = classes;
        this.max = max;
        index = new int[classes.length];
        this.values = values;

        for(int i = 8; i < classes.length; i++){
            max[i] = 1;
        }

        for(int i = 0; i < max.length; i++){
            if (max[i] == 0){
                hasNext = false;
                break;
            }
        }
    }

    private void increment(){
        boolean carry = true;

        for(int i = 0; i < classes.length; i++){
            if(index[i] < max[i] - 1){
                index[i] += 1;
                carry = false;
                break;
            } else {
                index[i] = 0;
            }
        }

        if(carry){
            hasNext = false;
        }
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public Object[] next() {
        Object[] objects = values.getValues(index, classes);
        increment();
        return objects;
    }
}
