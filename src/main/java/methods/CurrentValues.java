package methods;

import java.util.*;

public class CurrentValues {


    public Map<Class, List<Object>> getValues() {
        return values;
    }

    private Map<Class, List<Object>> values;

    public CurrentValues(List<ClassObjectPair> cop) {
        values = new HashMap<>();

        add(cop);
    }

    public CurrentValues(CurrentValues currentValues){
        values = new HashMap<>();

        for(Class key : currentValues.values.keySet()){
            values.put(key, new ArrayList<>(currentValues.values.get(key)));
        }
    }

    public CurrentValues(CurrentValues currentValues, List<ClassObjectPair> cop){
        values = new HashMap<>();

        for(Class key : currentValues.values.keySet()){
            values.put(key, new ArrayList<>(currentValues.values.get(key)));
        }

        add(cop);
    }

    public CurrentValues(CurrentValues currentValues1, CurrentValues currentValues2){
        values = new HashMap<>();

        for(Class key : currentValues1.values.keySet()){
            values.put(key, new ArrayList<>(currentValues1.values.get(key)));
        }

        for(Class key : currentValues2.values.keySet()){
            if(values.containsKey(key)){
                List<Object> list = values.get(key);

                for(Object val : currentValues2.values.get(key)){
                    if(!list.contains(val)){
                        list.add(val);
                    }
                }
            } else {
                values.put(key, new ArrayList<>(currentValues2.values.get(key)));
            }
        }
    }

    public boolean containsValues(Set<Object> objs){
        Set<Object> allValues = new HashSet<>();

        for(List<Object> lo :values.values()){
            allValues.addAll(lo);
        }

        return allValues.containsAll(objs);
    }

    public void add(List<ClassObjectPair> cop){
        if(cop == null){
            return;
        }

        for(ClassObjectPair co : cop){
            Class c = co.getC();
            Object o = co.getO();

            List<Object> objects;

            if(values.containsKey(c)){
                objects = values.get(c);
            } else {
                objects = new ArrayList<>();
                values.put(c, objects);
            }

            if(!objects.contains(o)){
                objects.add(o);
            }
        }
    }


    public Object[] getValues(int[] index, Class[] classes){
        Object[] objects = new Object[index.length];

        for(int i = 0; i < index.length; i++){
            objects[i] = values.get(classes[i]).get(index[i]);
        }

        return objects;

    }

    public Object[] getValues(int[] index, Class[] classes, Object newVal){
        Object[] objects = new Object[index.length];

        for(int i = 0; i < index.length; i++){
            if(index[i] < values.get(classes[i]).size()){
                objects[i] = values.get(classes[i]).get(index[i]);
            } else {
                objects[i] = newVal;
            }
        }

        return objects;

    }

    public List<Object> getObjectsOfClass(Class c){
        return values.get(c);
    }

    public boolean canSupply(MethodContainer methodContainer){
        return values.keySet().contains(methodContainer.getInputClasses());
    }

    public AllMethodInputs getPossibleMethodInputs(MethodContainer methodContainer){
        return getPossibleMethodInputs(methodContainer.getParameterTypes());
    }

    public AllMethodInputs getPossibleMethodInputs(Class[] classes){
        return new AllMethodInputs(classes, this);
    }


    public IterationMethodInputs getIterationMethodInputs(MethodContainer methodContainer, Class newValueClass, Object newValue){
        return new IterationMethodInputs(methodContainer.getParameterTypes(), this, newValueClass, newValue);
    }
    public IterationMethodInputs getIterationMethodInputs(Class[] classes, Class newValueClass, Object newValue){
        return new IterationMethodInputs(classes, this, newValueClass, newValue);
    }

    public Set<Class> keySet(){
        return values.keySet();
    }

    public void remove(CurrentValues cv){
        for(Class key : this.values.keySet()){
            if(cv.values.containsKey(key)){
                for(Object value : cv.values.get(key)){
                    if(this.values.get(key).contains(value)){
                        this.values.get(key).remove(value);
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "CurrentValues{" +
                "values=" + values +
                '}';
    }

}
