package methods;

import cache.Cache;
import cache.InfinityCache;
import cache.LRUCache;
import filters.NumericMethodPredicate;
import main.G;

import java.io.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

public class MethodContainer implements Serializable {

    private static final long serialVersionUID=4242424242424242L;

    private transient Method method;

    private Set<Class> inputClasses;
    private Set<Class> outputClasses;

    private Class returnType;
    private Class[] parameterTypes;

    private boolean isStatic;
    transient private Class declaringClass;

    private String jarFilePath;
    private String className;

    private String methodName;

    private boolean helperBool;

    private boolean probablyDet;



    private String name;

    private Set<List<Object>> inputOutputSet;

    public Map<List<Object>, List<Class>> getIoToType() {
        return ioToType;
    }

    public Map<List<Object>, Integer> getIoToTemplate() {
        return ioToTemplate;
    }

    private Map<List<Object>, List<Class>> ioToType;
    private Map<List<Object>, Integer> ioToTemplate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MethodContainer(Method method, String jarFilePath, String className){
        name = method.getName();

        probablyDet = true;

        inputOutputSet = new HashSet<>();
        ioToType = new HashMap<>();
        ioToTemplate = new HashMap<>();


        methodName = method.getName();


        this.method = method;

        this.jarFilePath = jarFilePath;
        this.className = className;

        inputClasses = new HashSet<>();

        returnType = method.getReturnType();
        parameterTypes = method.getParameterTypes();

        inputClasses = new HashSet<>(Arrays.asList(parameterTypes));
        outputClasses = new HashSet<>();


        if(!returnType.equals(void.class)){
            outputClasses.add(returnType);
        }

        for (Class parameterType : parameterTypes){
            if(!parameterType.isPrimitive()){
                outputClasses.add(parameterType);
            }
        }

        declaringClass = method.getDeclaringClass();

        int modifier = method.getModifiers();
        if(Modifier.isStatic(modifier)){
            isStatic = true;
        } else {
            isStatic = false;


            if(NumericMethodPredicate.isAcceptedClass(declaringClass)){
                inputClasses.add(declaringClass);
                outputClasses.add(declaringClass);
            }
        }

    }

    public void addInputOutput(List<Object> inputOutput, List<Class> type, int templateId){
        inputOutputSet.add(inputOutput);
        ioToType.put(inputOutput, type);
        ioToTemplate.put(inputOutput, templateId);
    }

    public Set<List<Object>> getInputOutputSet(){
        return inputOutputSet;
    }

    @Override
    public String toString() {
        return "method{" +
                "name = " + name +
                ", returnType=" + returnType +
                ", parameterTypes=" + Arrays.toString(parameterTypes) +
                ", jarFilePath=" + jarFilePath +
                ", className=" + className + "}\n";
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Set<Class> getInputClasses() {
        return inputClasses;
    }

    public void setInputClasses(Set<Class> inputClasses) {
        this.inputClasses = inputClasses;
    }

    public Set<Class> getOutputClasses() {
        return outputClasses;
    }

    public void setOutputClasses(Set<Class> outputClasses) {
        this.outputClasses = outputClasses;
    }

    public Class getReturnType() {
        return returnType;
    }

    public void setReturnType(Class returnType) {
        this.returnType = returnType;
    }

    public Class[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public Class getDeclaringClass() {
        return declaringClass;
    }

    public void setDeclaringClass(Class declaringClass) {
        this.declaringClass = declaringClass;
    }

    public String getJarFilePath() {
        return jarFilePath;
    }


    public String getClassName() {
        return className;
    }

    public boolean loadMethod(){

        if(!jarFilePath.equals("")){
            File jar = new File(jarFilePath);
            URL[] urls = new URL[1];
            try {
                urls[0] = jar.toURI().toURL();
            } catch (Exception e) {
                e.printStackTrace();
            }

            URLClassLoader uc = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());

            try {
                Class c = Class.forName(className, false, uc);
                method = c.getMethod(methodName, parameterTypes);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
                return false;
            }
        } else{
            try {
                Class<?> c = Class.forName(className);
                method = c.getMethod(methodName, parameterTypes);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
                return false;
            }
        }
        return true;
    }

    public void setHelper(boolean bool){
        this.helperBool = bool;
    }

    public boolean getHelper(){
        return this.helperBool;
    }

    public boolean isProbablyDet() {
        return probablyDet;
    }

    public void setProbablyDet(boolean probablyDet) {
        this.probablyDet = probablyDet;
    }
}
