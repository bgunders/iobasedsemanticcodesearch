package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Scrapper {

    private final static String apiClassIndex = "https://docs.oracle.com/javase/8/docs/api/allclasses-frame.html";

    /*
    read java online doc for list of class names.
     */
    public static ArrayList<String> getStdClasses(){
        ArrayList<String> classes = new ArrayList<>();

        try{
            URL url = new URL(apiClassIndex);

            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String s, className, classPath;
            String[] classNameSplit, pathSplit1, pathSplit2;

            while((s = in.readLine()) != null){
                if(s.contains("\" title=\"class in ")){
                    classNameSplit = s.split("target=\"classFrame\">");
                    pathSplit1 = s.split("\" title=\"class in ");

                    if(classNameSplit.length == 2 && pathSplit1.length == 2){
                        className = classNameSplit[1].replace("</a></li>", "");
                        pathSplit2 = pathSplit1[1].split("\" target=\"classFrame\">");

                        classPath = pathSplit2[0];

                        classes.add(classPath + "." + className);
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }
}
