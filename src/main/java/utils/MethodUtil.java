package utils;

import methods.MethodContainer;

import java.util.*;

public class MethodUtil {


    public static Map<List<Class>, Set<MethodContainer>> getParameterMethodMap(List<MethodContainer> methods) {
        Map<List<Class>, Set<MethodContainer>> methodMap;
        methodMap = new HashMap<>();

        for(int i = 0; i < methods.size(); i++){
            MethodContainer method = methods.get(i);

            List<Class> parameterTypes = new ArrayList<>(Arrays.asList(method.getParameterTypes()));
            Class returnType = method.getReturnType();



            for(Class parameterType : parameterTypes){
                if(!parameterType.isPrimitive()){
                    List<Class> key = new ArrayList<>(parameterTypes);
                    key.add(parameterType);

                    if(methodMap.containsKey(key)){
                        methodMap.get(key).add(method);
                    } else {
                        Set<MethodContainer> methodSet = new HashSet<>();
                        methodSet.add(method);
                        methodMap.put(key, methodSet);
                    }
                }
            }

            List<Class> key = new ArrayList<>(parameterTypes);
            key.add(returnType);

            if(methodMap.containsKey(key)){
                methodMap.get(key).add(method);
            } else {
                Set<MethodContainer> methodSet = new HashSet<>();
                methodSet.add(method);
                methodMap.put(key, methodSet);
            }
        }
        return methodMap;
    }
}
