package utils;

import methods.MethodContainer;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Util {


    public static Object[] deepCopy(Object[] o) throws IOException, ClassNotFoundException {
        //begin https://stackoverflow.com/questions/64036/how-do-you-make-a-deep-copy-of-an-object
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);


        oos.writeObject(o);
        oos.flush();
        oos.close();
        bos.close();
        byte[] byteData = bos.toByteArray();


        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
        Object[] oCopy = (Object[]) new ObjectInputStream(bais).readObject();
        //end https://stackoverflow.com/questions/64036/how-do-you-make-a-deep-copy-of-an-object
        return oCopy;
    }

    public static Object deepCopy(Object o) throws IOException, ClassNotFoundException {
        //begin https://stackoverflow.com/questions/64036/how-do-you-make-a-deep-copy-of-an-object
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);


        oos.writeObject(o);
        oos.flush();
        oos.close();
        bos.close();
        byte[] byteData = bos.toByteArray();


        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
        Object oCopy = (Object) new ObjectInputStream(bais).readObject();
        //end https://stackoverflow.com/questions/64036/how-do-you-make-a-deep-copy-of-an-object
        return oCopy;
    }


    public static void serializeMethods(List<MethodContainer> methods){
        FileOutputStream fileOutputStream = null;
        try {
            //https://www.javatpoint.com/serialization-in-java

            fileOutputStream = new FileOutputStream("methods.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(methods);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileOutputStream != null){

            }
        }
    }

    public static List<MethodContainer> deserializeMethods(){
        List<MethodContainer> methods = null;
        FileInputStream fileInputStream = null;
        try {
            //https://www.javatpoint.com/serialization-in-java

            fileInputStream = new FileInputStream("methods.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            methods = (List<MethodContainer>) objectInputStream.readObject();

            for(MethodContainer method : methods){
                method.loadMethod();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fileInputStream != null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return methods;
    }

    public static String primitiveArrayToString(Object arr){

        if(arr == null){
            return "null";
        }

        Class c = arr.getClass();

        while(c.isArray()){
            c = c.getComponentType();
        }

        if (c  == char.class || c == Character.class) {
            if(arr.getClass().isArray()){

                StringBuilder sb = new StringBuilder();

                sb.append("[");

                if(0 < Array.getLength(arr)){
                    sb.append(primitiveArrayToString(Array.get(arr, 0)));
                }

                for(int i = 1; i < Array.getLength(arr); i++){
                    sb.append(",");
                    sb.append(primitiveArrayToString(Array.get(arr, i)));
                }

                sb.append("]");
                return sb.toString();

            } else {
                return "'" + arr + "'";
            }
        } else {
            Object[] oArr = {arr};
            String oArrString = Arrays.deepToString(oArr);
            return oArrString.substring(1, oArrString.length() -1);
        }
    }
}
