package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HashUtils {

    /*
    Convert Arrays to lists since lists base equality and hash only on their elements. Other types can
    be left like they are.
    Examples: int -> int
    double[] -> List<Double>
    Double[] -> List<Double>
    double[][] -> List<List<Double>>
    Downside: double[] and Double[] map to List<Double>.
     */
    public static <T> Object toHashable(T[] obj){
        List<Object> ret = Arrays.stream(obj).collect(Collectors.toList());

        for(int i = 0; i < ret.size(); i++){
            Object r = ret.get(i);

            if(r == null){
                continue;
            }

            if(r.getClass() == byte[].class){
                ret.set(i, toHashable((byte[]) r));
            }
            else if(r.getClass() == short[].class){
                ret.set(i, toHashable((short[]) r));
            }
            else if(r.getClass() == int[].class){
                ret.set(i, toHashable((int[]) r));
            }
            else if(r.getClass() == long[].class){
                ret.set(i, toHashable((long[]) r));
            }
            else if(r.getClass() == float[].class){
                ret.set(i, toHashable((float[]) r));
            }
            else if(r.getClass() == double[].class){
                ret.set(i, toHashable((double[]) r));
            }
            else if(r.getClass() == boolean[].class){
                ret.set(i, toHashable((boolean[]) r));
            }
            else if(r.getClass() == char[].class){
                ret.set(i, toHashable((char[]) r));
            }
            else if(r.getClass().isArray()){
                ret.set(i, toHashable((Object[]) r));
            } else {
                ret.set(i, toHashable(r));
            }
        }
        return ret;
    }

    public static Object toHashable(byte[] obj){
        List<Byte> ret = new ArrayList<>();
        for(int i = 0; i < obj.length; i++){
            ret.add(obj[i]);
        }
        return ret;
    }

    public static Object toHashable(short[] obj){
        List<Short> ret = new ArrayList<>();
        for(int i = 0; i < obj.length; i++){
            ret.add(obj[i]);
        }
        return ret;
    }

    public static Object toHashable(int[] obj){
        List<Integer> ret = Arrays.stream(obj).boxed().collect(Collectors.toList());
        return ret;
    }

    public static Object toHashable(long[] obj){
        List<Long> ret = Arrays.stream(obj).boxed().collect(Collectors.toList());
        return ret;
    }

    public static Object toHashable(float[] obj){
        List<Float> ret = new ArrayList<>();
        for(int i = 0; i < obj.length; i++){
            ret.add(obj[i]);
        }
        return ret;
    }

    public static Object toHashable(double[] obj){
        List<Double> ret = Arrays.stream(obj).boxed().collect(Collectors.toList());
        return ret;
    }

    public static Object toHashable(boolean[] obj){
        List<Boolean> ret = new ArrayList<>();
        for(int i = 0; i < obj.length; i++){
            ret.add(obj[i]);
        }
        return ret;
    }

    public static Object toHashable(char[] obj){
        List<Character> ret = new ArrayList<>();
        for(int i = 0; i < obj.length; i++){
            ret.add(obj[i]);
        }
        return ret;
    }

    public static Object toHashable(Object obj){
        if(obj.getClass().isArray()){
            List<Object> a = null;

            Class base = obj.getClass();

            while(base.isArray()){
                base = base.getComponentType();
            }

            if(base == int.class){
                a = (List<Object>) toHashable((int[])obj);
            } else if(base == byte.class){
                a = (List<Object>) toHashable((byte[])obj);
            } else if(base == short.class){
                a = (List<Object>) toHashable((short[])obj);
            } else if(base == long.class){
                a = (List<Object>) toHashable((long[])obj);
            } else if(base == float.class){
                a = (List<Object>) toHashable((float[])obj);
            } else if(base == double.class){
                a = (List<Object>) toHashable((double[])obj);
            }  else if(base == boolean.class){
                a = (List<Object>) toHashable((boolean[])obj);
            } else if(base == char.class){
                a = (List<Object>) toHashable((char [])obj);
            }  else {
                a = (List<Object>) toHashable((Object[])obj);
            }

            List<Object> ret = new ArrayList<>();

            for(Object b : a){
                ret.add(toHashable(b));
            }
            return ret;
        }
        return obj;
    }
}
