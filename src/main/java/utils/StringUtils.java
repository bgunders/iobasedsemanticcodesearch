package utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    private static Pattern packagePattern = Pattern.compile("package\\s+[^;]+;");

    /*
    returns true if the string contains a package declaration.
     */
    public static boolean isPackageDeclaration(String str){
        Matcher matcher = packagePattern.matcher(str);
        return matcher.find();
    }

    /*
    returns the package name.
     */
    public static String extractPackage(String str){
        str = str.replace("package ", "");
        String[] split = str.split(";");
        return split[0];
    }

    /*
    Get path to the root folder of a project based on package and file path.
     */
    public static String packageToRootPath(String str, File file){
        str = str.replace(".", File.separator);
        return replaceLast(file.getParent(), str, "");
    }

    public static String replaceLast(String str1, String str2, String replacement){
        String reverse1 = new StringBuilder(str1).reverse().toString();
        String reverse2 = new StringBuilder(str2).reverse().toString();
        String reverseReplaced = reverse1.replace(reverse2, replacement);
        return new StringBuilder(reverseReplaced).reverse().toString();
    }
}
