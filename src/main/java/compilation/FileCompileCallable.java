package compilation;

import utils.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.PatternSyntaxException;

public class FileCompileCallable implements Callable<Boolean> {

    private File f;

    public FileCompileCallable(File f){
        this.f = f;
    }

    @Override
    public Boolean call() throws Exception {
        Set<String> rootPaths  = new HashSet<>();

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

        projectsAux(rootPaths, f);


        List<JavaSrcCompiler> compilers = new ArrayList<>();

        for(String path : rootPaths){
            JavaSrcDir javaSrcDir = new JavaSrcDir(new File(path));

            JavaSrcCompiler compiler = javaSrcDir.getCompiler();

            compilers.add(compiler);
        }

        List<Future<Boolean>> futures = executorService.invokeAll(compilers);

        for(Future future : futures){
            future.get();
        }


        return null;
    }

    private String projectsAux(Set<String> rootFiles, File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            if(files == null){
                return "";
            }
            for(File f : files){
                String res = projectsAux(rootFiles, f);
            }
            return "";
        } else {
            if(file.getName().endsWith(".java")){
                Scanner sc = null;
                try {
                    sc = new Scanner(file);
                    while(sc.hasNextLine()){
                        String line = sc.nextLine();
                        if(StringUtils.isPackageDeclaration(line)){
                            try{
                                String pack = StringUtils.extractPackage(line);
                                pack = StringUtils.packageToRootPath(pack, file);
                                rootFiles.add(pack);
                                return pack;
                            } catch (PatternSyntaxException e){
                                System.out.println(e);
                            }
                        } if (line.contains("import")){
                            break;
                        } if (line.contains("public class")){
                            break;
                        }
                    }
                } catch (FileNotFoundException e) {
                } finally{
                    if(sc != null){
                        sc.close();
                    }
                }
                return file.getParent();
            }

        }
        return "";
    }

    private void doCompilation(ExecutorService executorService, List<JavaSrcCompiler> compilers) {
        try {
            List<Future<Boolean>> futures = executorService.invokeAll(compilers);

            for(Future future : futures){
                try {
                    future.get(1, TimeUnit.SECONDS);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        compilers.clear();
    }
}
