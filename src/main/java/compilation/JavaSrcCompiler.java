package compilation;

import utils.NullWriter;

import javax.tools.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class JavaSrcCompiler implements Callable<Boolean> {

    //https://stackoverflow.com/questions/1281229/how-to-use-jaroutputstream-to-create-a-jar-file
    //http://www.java2s.com/Code/Java/File-Input-Output/CreateJarfile.htm

    private JavaSrcDir javaSrcDir;
    private File srcDir;
    private String srcPath;
    private String name;
    private File jarFile;
    private File rootDir;

    private boolean success = false;

    private JavaSrcCompiler(){ }

    public JavaSrcCompiler(JavaSrcDir javaSrcDir){
        this.javaSrcDir = javaSrcDir;
        this.srcDir = javaSrcDir.getDir();
        this.srcPath = srcDir.getPath();
            name = "jars/" + File.separator + srcDir.getAbsolutePath().replace(File.separator, "_") + ".jar";
    }

    public boolean isSuccess(){
        return success;
    }

    public String getJarName(){
        return name;
    }

    public File getJarFile(){
        return jarFile;
    }

    public JavaSrcDir getJavaSrcDir() {
        return javaSrcDir;
    }

    public Boolean call()
    {

        JarOutputStream target = null;
        try {
            if(!compile()){
                return false;
            }

            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
            //manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, "src.main.Main.class");
            target = new JarOutputStream(new FileOutputStream(name), manifest);
            File inputDirectory = srcDir;
            for (File nestedFile : inputDirectory.listFiles())
                add("", nestedFile, target);



            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(target != null){
                try {
                    target.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    private void add(String parents, File source, JarOutputStream target) throws IOException
    {
        BufferedInputStream in = null;
        try
        {
            String name = (parents + source.getName()).replace("\\", "/");

            if (source.isDirectory())
            {
                if (!name.isEmpty())
                {
                    if (!name.endsWith("/"))
                        name += "/";
                    JarEntry entry = new JarEntry(name);
                    entry.setTime(source.lastModified());
                    target.putNextEntry(entry);
                    target.closeEntry();
                }
                for (File nestedFile : source.listFiles())
                    add(name, nestedFile, target);
                return;
            }

            JarEntry entry = new JarEntry(name);
            entry.setTime(source.lastModified());
            target.putNextEntry(entry);
            in = new BufferedInputStream(new FileInputStream(source));

            byte[] buffer = new byte[1024];
            while (true)
            {
                int count = in.read(buffer);
                if (count == -1)
                    break;
                target.write(buffer, 0, count);
            }
            target.closeEntry();
        }
        finally
        {
            if (in != null)
                in.close();
        }
    }

    private boolean compile() throws IOException {
        //https://stackoverflow.com/questions/10076868/java-programmatically-compile-jar
        //https://www.logicbig.com/tutorials/core-java-tutorial/java-se-compiler-api/java-compiler-api-intro.html
        //https://www.developer.com/design/an-introduction-to-the-java-compiler-api/
        //https://openjdk.java.net/groups/compiler/guide/compilerAPI.html

        List<File> files = new ArrayList<>();
        List<String> srcPaths = javaSrcDir.getSrcFiles();

        for(String path : srcPaths){
            files.add(new File(path));
        }

        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnosticCollector = new DiagnosticCollector<>();

        StandardJavaFileManager standardJavaFileManager = javaCompiler.getStandardFileManager(diagnosticCollector, null, null);

        Iterable<? extends  JavaFileObject> sources = standardJavaFileManager.getJavaFileObjectsFromFiles(files);


        JavaCompiler.CompilationTask compilationTask = javaCompiler.getTask(new NullWriter(), standardJavaFileManager, diagnosticCollector, null, null, sources);
        boolean success = compilationTask.call();

        return success;
    }

}
