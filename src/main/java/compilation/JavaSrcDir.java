package compilation;

import main.Main;
import methods.MethodContainer;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class JavaSrcDir {
    private File rootDir;

    private JavaSrcDir(){}

    public JavaSrcDir(String rootString){
        this(new File(rootString));
    }

    public JavaSrcDir(File rootFile){
        this.rootDir = rootFile;
    }


    public JavaSrcCompiler getCompiler(){
        JavaSrcCompiler javaSrcCompiler = new JavaSrcCompiler(this);
        return javaSrcCompiler;
    }



    public File getDir(){
        return rootDir;
    }

    public List<String> getClassNames(){
        List<String> classNames = new ArrayList<>();
        Object[] a = new Object[1];
        classNames.toArray(a);
        Stack<File> stack1 = new Stack<>();
        Stack<String> stack2 = new Stack<>();

        File currentFile = rootDir;
        String currentPrefix = "";

        if(currentFile.isDirectory()){
            stack1.push(currentFile);
            stack2.push(currentPrefix);

        }

        while(!stack1.isEmpty()){
            currentFile = stack1.pop();
            currentPrefix = stack2.pop();
            for (File file : currentFile.listFiles()){
                String fileName = file.getName();

                if(file.isDirectory()){
                    stack1.push(file);
                    stack2.push(currentPrefix + fileName + ".");
                } else if(fileName.endsWith(".java")) {
                    String name = fileName.replace(".java", "");
                    classNames.add(currentPrefix + (name));
                }
            }
        }
        return classNames;
    }

    public List<String> getSrcFiles(){
        List<String> classNames = new ArrayList<>();
        Object[] a = new Object[1];
        classNames.toArray(a);
        Stack<File> stack1 = new Stack<>();
        Stack<String> stack2 = new Stack<>();

        File currentFile = rootDir;
        String currentPrefix = rootDir.getAbsolutePath() + File.separator;

        if(currentFile.isDirectory()){
            stack1.push(currentFile);
            stack2.push(currentPrefix);

        }

        while(!stack1.isEmpty()){
            currentFile = stack1.pop();
            currentPrefix = stack2.pop();
            for (File file : currentFile.listFiles()){
                String fileName = file.getName();

                if(file.isDirectory()){
                    if(!file.getName().equalsIgnoreCase("test")){
                        stack1.push(file);
                        stack2.push(currentPrefix + fileName + File.separator);
                    }
                } else if(fileName.endsWith(".java")) {
                    classNames.add(currentPrefix + (fileName));
                }
            }
        }
        return classNames;
    }

    /*
    public List<String> getLoadableClasses(){
        List<String> classes = this.getClassNames();

        List<String> loadableClasses = new ArrayList<>();

        for (String classPath : classes){

            try {
                Class<?> c = Class.forName(classPath);
                loadableClasses.add(classPath);
            } catch (Exception e) {
                //System.out.println(e);
            } catch (Error e){
                //System.out.println(e);
            }
        }

        return loadableClasses;
    }

    public List<MethodContainer> getAcceptedMethods(){
        List<String> classes = getLoadableClasses();

        List<MethodContainer> r = new ArrayList<>();

        for (String classPath : classes){
            try {
                Class<?> c = Class.forName(classPath);

                Method[] methods = c.getMethods();

                for (Method method : methods){
                    int mod = method.getModifiers();

                    if (Modifier.isPublic(mod)){
                        Class ret = method.getReturnType();
                        Class[] params = method.getParameterTypes();

                        if(isAcceptedClass(ret) || ret.equals(void.class)){
                            boolean accept = true;
                            boolean atLeastOneNonPrimitive = false;

                            if(params.length > 0){
                                for (Class param : params){
                                    if(!isAcceptedClass(param)){
                                        accept = false;
                                        break;
                                    }
                                    if(!param.isPrimitive()){
                                        atLeastOneNonPrimitive = true;
                                    }
                                }
                            } else {
                                accept = false;
                            }

                            if(!Modifier.isStatic(mod) && !method.getDeclaringClass().isPrimitive()){
                                accept = true;
                            }

                            if(accept){
                                if(ret.equals(void.class)){
                                    if (atLeastOneNonPrimitive){
                                        r.add(new MethodContainer(method));
                                    }
                                } else {
                                    r.add(new MethodContainer(method));
                                }
                            }
                        }
                    }
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return r;
    }

    private static boolean isAcceptedClass(Class c){
        while (c.isArray()){
            c = c.getComponentType();
        }

        if(c.isPrimitive()){
            return true;
        }

        for (Class accept : Main.additionalClasses){
            if (c.equals(accept)) {
                return true;
            }
        }

        return false;
    }
     */
}
