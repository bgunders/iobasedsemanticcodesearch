package compilation;

import process.ProcessExecutor;
import utils.StringUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.PatternSyntaxException;

public class JavaMultipleSrcDir extends ProcessExecutor {

    private File rootDir;

    List<JavaSrcDir> compiles;
    List<File> jarFiles;
    List<JavaSrcCompiler> compilers;


    public JavaMultipleSrcDir(String rootString){
        this(new File(rootString));
    }

    public JavaMultipleSrcDir(File rootFile){
        this.rootDir = rootFile;
    }


    private String projectsAux(Set<String> rootFiles, File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            if(files == null){
                return "";
            }
            for(File f : files){
                String res = projectsAux(rootFiles, f);
            }
            return "";
        } else {
            if(file.getName().endsWith(".java")){
                Scanner sc = null;
                try {
                    sc = new Scanner(file);
                    while(sc.hasNextLine()){
                        String line = sc.nextLine();
                        if(StringUtils.isPackageDeclaration(line)){
                            try{
                                String pack = StringUtils.extractPackage(line);
                                pack = StringUtils.packageToRootPath(pack, file);
                                rootFiles.add(pack);
                                return pack;
                            } catch (PatternSyntaxException e){
                                System.out.println(e);
                            }
                        } if (line.contains("import")){
                            break;
                        } if (line.contains("public class")){
                            break;
                        }
                    }
                } catch (FileNotFoundException e) {
                } finally{
                    if(sc != null){
                        sc.close();
                    }
                }
                return file.getParent();
            }

        }
        return "";
    }

    private Set<File> getProjectRootDirs(){
        Set<String> rootPaths  = new HashSet<>();
        projectsAux(rootPaths, rootDir);

        Set<File> files = new HashSet<>();
        for(String path : rootPaths){
            files.add(new File(path));
        }

        return files;
    }


    public void compileOptimized(){
        File jars = new File("jars");

        String exec = "java -jar target/CompilerProcess.jar";

        if(!jars.exists()){
            jars.mkdir();
        }

        int numThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        if(rootDir.isDirectory()){
            int port = 58494;

            startProcess(exec, port);


            File[] files = rootDir.listFiles();
            int filesLen = files.length;

            int i = 0;
            for(File f : files){
                i++;
                System.out.print("\r" + i + " / " + filesLen + " : " +f.getName());

                try {
                    objectOutputStream.writeObject(f);
                    int status = (int) objectInputStream.readObject();

                    if(status == 1){
                        restartProcess(exec);
                    }

                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }


            }
        } else {
            System.out.println("Must enter a directory to compile");
            System.exit(1);
        }
        closeAll();
    }

    private void doCompilation(ExecutorService executorService, List<JavaSrcCompiler> compilers) {
        try {
            List<Future<Boolean>> futures = executorService.invokeAll(compilers);

            for(Future future : futures){
                try {
                    future.get(1, TimeUnit.SECONDS);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        compilers.clear();
    }

    public void compile(){
        compiles = new ArrayList<>();
        jarFiles = new ArrayList<>();
        compilers = new ArrayList<>();

        System.out.println("ye");
        getProjectRootDirs();
        System.out.println("yo");

        for (File file : getProjectRootDirs()){
            JavaSrcDir javaSrcDir = new JavaSrcDir(file);
            compilers.add(javaSrcDir.getCompiler());
        }



        int numThreads = Runtime.getRuntime().availableProcessors();
        System.out.println(numThreads);

        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        try {
            executorService.invokeAll(compilers);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executorService.shutdown();

        for(JavaSrcCompiler compiler : compilers){
            if(compiler.isSuccess()){
                compiles.add(compiler.getJavaSrcDir());
                jarFiles.add(compiler.getJarFile());
            }
        }

        System.out.println(compiles.size());
    }



}
