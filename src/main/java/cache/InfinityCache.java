package cache;

import java.io.Serializable;
import java.util.*;


public class InfinityCache implements Cache, Serializable {
    private static final long serialVersionUID=29847234873424L;

    private Map cache;

    public InfinityCache(){
        cache = new HashMap();
    }

    @Override
    public void add(Object k, Object v) {
        Object[] a = {k};
        String key = Arrays.deepToString(a);
        cache.put(key, v);
    }

    @Override
    public Object get(Object k) {
        Object[] a = {k};
        String key = Arrays.deepToString(a);


        if(cache.containsKey(key)){
            return cache.get(key);
        }
        return null;
    }

    @Override
    public boolean contains(Object k) {
        return cache.containsKey(k);
    }

    @Override
    public Set<Object> keys() {
        return cache.keySet();
    }

    @Override
    public Collection<Object> values() {
        return cache.values();
    }

    @Override
    public String toString() {
        return "InfinityCache{" +
                "cache=" + cache +
                '}';
    }
}
