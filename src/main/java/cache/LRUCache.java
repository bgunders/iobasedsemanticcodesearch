package cache;

import java.io.Serializable;
import java.util.*;

public class LRUCache implements Cache, Serializable {
    private static final long serialVersionUID=4268765132765354374L;

    int size;

    private Map<Object, Object> cache = new HashMap<>();

    private Queue<Object> queue = new LinkedList<>();


    public LRUCache(){
        this.size = 128;
    }

    public LRUCache(int size){
        this.size = size;
    }

    @Override
    public void add(Object k, Object v) {
        Object[] a = {k};
        String key = Arrays.deepToString(a);
        if(cache.containsKey(key)){
            cache.put(key, v);
            queue.remove(key);
            queue.add(key);
        } else {
            if(cache.size() > size){
                Object rem = queue.poll();
                cache.remove(rem);

                cache.put(key, v);
            }
        }
    }

    @Override
    public Object get(Object k) {
        Object[] a = {k};
        String key = Arrays.deepToString(a);
        Object ret = null;

        if(cache.containsKey(key)){
            ret = cache.get(key);

            queue.remove(key);
            queue.add(key);
        }

        return ret;
    }

    @Override
    public boolean contains(Object k) {
        return cache.containsKey(k);
    }

    @Override
    public Set<Object> keys() {
        return cache.keySet();
    }

    @Override
    public Collection<Object> values() {
        return cache.values();
    }

    @Override
    public String toString() {
        return "LRUCache{" +
                "size=" + size +
                ", cache=" + cache +
                ", queue=" + queue +
                '}';
    }
}
