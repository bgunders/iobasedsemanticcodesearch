package cache;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface Cache {

    public void add(Object k, Object v);

    public Object get(Object k);

    public boolean contains(Object k);

    public Set<Object> keys();

    public Collection<Object> values();
}
