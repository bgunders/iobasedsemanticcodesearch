package userinput;

import compilation.JavaMultipleSrcDir;
import composition.MethodComposition;
import filters.NumericMethodPredicate;
import inputoutputMatching.InputOutputMatcher;
import main.G;
import main.InputOutputMatcherTestClone;
import methods.ClassObjectPair;
import methods.CurrentValues;
import methods.MethodContainer;
import methods.MethodExtractor;
import parser.Parser;
import parser.PrimitiveArrayParser;
import utils.HashUtils;

import java.io.File;
import java.lang.reflect.Array;
import java.util.*;

public class UserMethodMatcher {

    private InputOutputMatcher inputOutputMatcher;
    private Set<Integer> methods;

    private MethodContainer randomMethod;

    String helpString = "";

    private MethodComposition methodComposition = null;

    public UserMethodMatcher(InputOutputMatcher inputOutputMatcher, Set<Integer> methods){
        this.inputOutputMatcher = inputOutputMatcher;
        this.methods = methods;


        StringBuilder help = new StringBuilder();
        help.append("n / new -> Set possible methods to all, meaning you can search through all methods.\n");
        help.append("m / match -> Start searching for methods in current set of possible methods. When searching for methods matched methods will be the new set of possible methods.");
        help.append("First the type needs to be entered. Separate each type with a space. Prepend \"[\" for every dimension of an array. Examples: \"i\" is for int, \"[I\" for one dimensional Integer array. All types:\n");
        help.append(map);
        help.append("\n");
        help.append("p -> prints all possible methods. Meaning this can be used to print the matched methods.\n");
        help.append("g -> prints all possible methods grouped.\n");
        help.append("e -> exit this program\n");
        help.append("r -> select a random method and print it\n");
        help.append("t -> lists all test and lets the user select which one to run.\n");
        help.append("c -> attempt to compile all repositories in a directory and add the compiled jars into jars/\n");
        help.append("s -> print out how many methods are in possible methods and how many groups.\n");
        help.append("l -> print out all templates.\n");
        help.append("h -> print help (this). \n");
        help.append("pc -> pre compute based on jars in jars/ and java8src\n");
        help.append("comp -> search for method composition\n");
        help.append("auto -> toggle automatic type detection for match.");
        helpString = help.toString();
    }

    public void start(){
        boolean auto = false;

        String in = "";
        String out = "";

        List<ClassObjectPair> inputs;
        List<ClassObjectPair> outputs;

        CurrentValues currentValues;

        Scanner sc = new Scanner(System.in);
        Parser parser = new PrimitiveArrayParser();

        Set<Integer> possibleMethods = new HashSet<>(methods);

        while (true){
            boolean invokeRandom = false;

            inputs = new ArrayList<>();
            int inputNumber = 0;

            boolean exit = false;
            boolean nextLoop = true;

            while(nextLoop){
                System.out.print("n[ew], m[atch], p[rint], g[print], e[xit], r[andom], t[ests], c[ompile], s[tats] [temp]l[ates], h[elp], comp, a[uto], pc: ");
                in = sc.nextLine();
                switch (in){
                    case "n":
                    case "new":
                        possibleMethods = new HashSet<>(methods);
                        System.out.println();
                        break;
                    case "m":
                    case "match":
                        nextLoop = false;
                        break;
                    case "p":
                    case "print":
                        System.out.println(inputOutputMatcher.getMethods(possibleMethods));
                        break;
                    case "g":
                    case "gprint":
                        Map<Set<List<Object>>, Set<Integer>> groups = inputOutputMatcher.getGroupFunction().apply(possibleMethods);
                        int group = 0;
                        for(Set<List<Object>> key : groups.keySet()){
                            System.out.println("group #" + group + " size = " + groups.get(key).size());
                            System.out.println(inputOutputMatcher.getMethods(groups.get(key)));
                            group++;
                        }
                        break;
                    case "e":
                    case "exit":
                        nextLoop = false;
                        exit = true;
                        break;

                    case "r":
                    case "random":
                        int random = (int) (Math.random() * methods.size());
                        List<Integer> methodList = new ArrayList<>(methods);
                        int randomInt = methodList.get(random);
                        Set<Integer> randomIntSet = new HashSet<>();
                        randomIntSet.add(randomInt);
                        List<MethodContainer> randomMethodSet = new ArrayList<>(inputOutputMatcher.getMethods(randomIntSet));
                        randomMethod = randomMethodSet.get(0);
                        System.out.println(randomMethod);
                        break;
                    case "i":
                    case "invoke":
                        //possibleMethods = new HashSet<>();
                        //possibleMethods.add(randomMethod);

                        //invokeRandom = true;
                        //nextLoop = false;
                        break;
                    case "s":
                    case "stats":
                        Map<Set<List<Object>>, Set<Integer>> grouped = inputOutputMatcher.getGroupFunction().apply(possibleMethods);
                        System.out.println("num methods = " + possibleMethods.size());
                        System.out.println("num groups = " + grouped.keySet().size());
                        break;
                    case "l":
                    case "templates":
                        System.out.println(G.templates);
                        break;
                    case "comp":
                        try{
                            if(methodComposition == null){
                                System.out.println("Loading pre-computed data.");
                                methodComposition = MethodComposition.deserialize();
                            } else {
                                System.out.println("Data already loaded.");
                            }

                            int template = -1;
                            while(template == -1){
                                System.out.print("Template = ");
                                in = sc.nextLine();
                                for (ClassObjectPair cop : parser.parse(in)){
                                    if(cop.getC() == int.class){
                                        template = (int) cop.getO();
                                        if(template < 0 || template >= G.TEMPLATES.size()){
                                            template = -1;
                                        }
                                    }
                                }
                            }
                            List<Integer> templates = new ArrayList<>();
                            templates.add(template);

                            System.out.print("Types = ");
                            in = sc.nextLine();
                            String[] split = in.split(" ");

                            Class[] types = new Class[split.length];

                            for(int i = 0; i < split.length; i++){
                                types[i] = getClass(split[i]);
                            }
                            List<Object> ioPair = new ArrayList<>();


                            for(int i = 0; i < types.length; i++){
                                System.out.print("Entry (" + types[i] + ") #" + i + " = ");
                                in = sc.nextLine();
                                List<ClassObjectPair> parsed = parser.parse(in);

                                for(ClassObjectPair cop : parsed){
                                    if(cop.getC().equals(types[i])){
                                        ioPair.add(HashUtils.toHashable(cop.getO()));
                                    }
                                }
                            }
                            if(ioPair.size() != 1){
                                System.out.println("Only one out.");
                                break;
                            }
                            System.out.println(ioPair.get(0).getClass());
                            System.out.println(types[0]);
                            List<List<MethodContainer>> ret = methodComposition.findComposition(ioPair.get(0), types[0], Integer.MAX_VALUE, templates);
                            System.out.println(ret);
                            for(List<MethodContainer> r : ret){
                                System.out.println("--");
                                System.out.println(r);
                            }
                        } catch (Exception e){
                            System.out.println("ENTER to continue (input not correct format)");
                            continue;
                        } catch (Error e){
                            System.out.println("ENTER to continue (input not correct format)");
                            continue;
                        }
                        break;
                    case "a":
                    case "auto":
                        auto = !auto;
                        System.out.println("Auto is now " + auto);
                        System.out.println("Press Enter after being asked for an entry in the I/O pair to finish the I/O pair.");
                        break;
                    case "t":
                    case "test":
                        System.out.println("Plots are in python/images");
                        System.out.println("0 -> Most Test: non-primitives, group-size, parameter-count, different parameter sizes, type methods, random I/O experiments...");
                        System.out.println("1 -> Type vs Direct access time test.");
                        System.out.println("Test = ");
                        in = sc.nextLine();

                        if(in.equals("0")){
                            System.out.println("Most Test: non-primitives, group-size, parameter-count, different parameter sizes, type methods, random I/O experiments...");
                            InputOutputMatcherTestClone test = new InputOutputMatcherTestClone();
                            test.randomIO();
                        } else if(in.equals("1")){
                            System.out.println("Starting: Type vs Direct access time test.");
                            InputOutputMatcherTestClone test = new InputOutputMatcherTestClone();
                            test.classVsNoClassMapTest();
                        } else {
                            System.out.println("No such test found.");
                        }

                        break;
                    case "c":
                    case "compile":
                        System.out.print("Dir to compile = ");
                        in = sc.nextLine();
                        System.out.println("Compiling " + in);

                        File f = new File(in);

                        JavaMultipleSrcDir javaMultipleSrcDir = new JavaMultipleSrcDir(f);
                        System.out.println("Start compiling..");
                        javaMultipleSrcDir.compileOptimized();
                        System.out.println("Finished compiling.");
                        break;
                    case "pc":
                        System.out.println("Extracting Methods");
                        MethodExtractor methodExtractor = new MethodExtractor(new NumericMethodPredicate());
                        List<MethodContainer> methods = new ArrayList<>();
                        methodExtractor.getAllMethod(methods);
                        System.out.println("Extracted Methods, num = " + methods.size());
                        InputOutputMatcher tempIoMatcher = new InputOutputMatcher(methods);
                        tempIoMatcher.serialize();
                        this.methods = tempIoMatcher.methodMap.keySet();
                        this.inputOutputMatcher = tempIoMatcher;
                        System.out.println("Finished pc.");
                        break;
                    default:
                        System.out.println(helpString);
                        break;
                }
            }

            if(exit){
                break;
            }

            try{
                List<Object> ioPair = new ArrayList<>();
                Class[] types = null;

                if(auto){
                    List<ClassObjectPair> cops = autoIo(sc, parser);
                    types = new Class[cops.size()];

                    for (int i = 0; i < cops.size(); i++){
                        ioPair.add(HashUtils.toHashable(cops.get(i).getO()));
                        types[i] = cops.get(i).getC();
                    }

                } else {
                    System.out.print("Types = ");
                    in = sc.nextLine();
                    String[] split = in.split(" ");

                    types = new Class[split.length];

                    for(int i = 0; i < split.length; i++){
                        types[i] = getClass(split[i]);
                    }

                    System.out.println("Types = " + Arrays.deepToString(types));

                    for(int i = 0; i < types.length; i++){
                        System.out.print("Entry (" + types[i] + ") #" + i + " = ");
                        in = sc.nextLine();
                        List<ClassObjectPair> parsed = parser.parse(in);

                        for(ClassObjectPair cop : parsed){
                            if(cop.getC().equals(types[i])){
                                ioPair.add(HashUtils.toHashable(cop.getO()));
                                break;
                            }
                        }
                    }
                }

                System.out.println("ioPair = " + ioPair);

                Class[] inType = new Class[types.length - 1];

                for(int i = 0; i < types.length - 1; i++){
                    inType[i] = types[i];
                }

                Class outType = types[types.length - 1];

                long start = System.nanoTime();
                Set<Integer> matched = inputOutputMatcher.getIndexes(inType, outType, ioPair);
                long end = System.nanoTime();
                possibleMethods.retainAll(matched);
                System.out.println("Matched in (ns) " + (end - start));
            } catch (Exception e){
                System.out.println("ENTER to continue (input not correct format)");
                continue;
            } catch (Error e){
                System.out.println("ENTER to continue (input not correct format)");
                continue;
            }
        }
    }

    private List<ClassObjectPair> autoIo(Scanner sc, Parser parser){
        List<ClassObjectPair> ioPair = new ArrayList<>();

        String in = "auto";
        int i = 0;
        while(!in.equals("")){
            System.out.print("Entry #" + i + " = ");
            in = sc.nextLine();
            if(in.equals("")){
                break;
            }
            List<ClassObjectPair> parsed = parser.parse(in);
            ioPair.add(getAutoCop(parsed));
            i++;
        }
        System.out.println(ioPair);
        return ioPair;
    }

    private ClassObjectPair getAutoCop(List<ClassObjectPair> cops){
        Map<Class, ClassObjectPair> map = new HashMap<>();

        for(int i = 0; i < cops.size(); i++){
            Class type = cops.get(i).getC();
            while(type.isArray()){
                type = type.getComponentType();
            }

            map.put(type, cops.get(i));
        }

        Set<Class> types = map.keySet();


        Class[] pres = {int.class, double.class, boolean.class, char.class, long.class, short.class, byte.class, float.class};

        for (int i = 0; i < pres.length; i++){
            Class type = pres[i];
            if(types.contains(type)){
                return map.get(type);
            }
        }
        return null;
    }

    private static Map<String, Class> map = new HashMap<>();

    static {
        map.put("y", byte.class);
        map.put("Y", Byte.class);
        map.put("s", short.class);
        map.put("S", Short.class);
        map.put("i", int.class);
        map.put("I", Integer.class);
        map.put("l", long.class);
        map.put("L", Long.class);
        map.put("d", double.class);
        map.put("D", Double.class);
        map.put("c", char.class);
        map.put("C", Character.class);
        map.put("b", boolean.class);
        map.put("B", Boolean.class);
    }

    private Class getClass(String str){
        if(str.startsWith("[")){
            int dim = 0;
            for(int i = 0; i < str.length() - 1; i++){
                if(str.charAt(i) == '['){
                    dim++;
                }
            }
            String type = str.charAt(str.length() - 1) + "";
            int[] dimArr = new int[dim];


            return (Array.newInstance(map.get(type), dimArr).getClass());
        } else {
            return map.get(str);
        }
    }
}
