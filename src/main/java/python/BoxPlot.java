package python;

import java.io.*;

/*
Creates Box plots with python.
 */
public class BoxPlot extends Plot {

    /*
    Create box plot python program.
     */
    public BoxPlot(String s, int iter, String title, int mode){
        StringBuilder sb = new StringBuilder();

        sb.append("import matplotlib.pyplot as plt\n");
        sb.append("import numpy as np\n");
        sb.append("iter = "); sb.append(iter); sb.append("\n");
        sb.append("title = \""); sb.append(title); sb.append("\"\n");
        sb.append("a = "); sb.append(s); sb.append("\n");

        if(mode == 1 || mode == 0){
            sb.append("b = np.array(a)\n");
        }

        sb.append("plt.rcParams['figure.dpi'] = 300\n");
        sb.append("plt.rcParams['savefig.dpi'] = 300\n");



        if(mode == 0){
            sb.append("labels = [\"Type Filter\"]\n");
            sb.append("for i in range(iter - 1):\n");
            sb.append("    if i == 0:");
            sb.append("        labels.append(\"1 Pair\")\n");
            sb.append("    if i > 0:");
            sb.append("        labels.append(str(i+1) + \" Pairs\")\n");
        } else if(mode == 1){
            sb.append("labels = []\n");
            sb.append("for i in range(iter):\n");
            sb.append("    if i == 0:");
            sb.append("        labels.append(\"1 Pair\")\n");
            sb.append("    if i > 0:");
            sb.append("        labels.append(str(i+1) + \" Pairs\")\n");
        } else if(mode == 2){
            sb.append("labels = [\"All Types\", \"Boolean Return\", \"Non Boolean Return\"]\n");
        }


        sb.append("fig, ax = plt.subplots()\n");

        sb.append("ax.set_title(title)\n");

        if(mode == 0 || mode == 1){
            sb.append("bp = ax.boxplot(b, labels=labels, showmeans=True)\n");

            sb.append("for l in bp['medians']:\n");
            sb.append("    x,y = l.get_xydata()[1]\n");
            sb.append("    ax.annotate(str(y), [x,y])\n");
        } else if(mode == 2){
            sb.append(("bp = ax.boxplot(a[0], positions = [1], labels = [labels[0]], showmeans = True)\n"));
            sb.append("for l in bp['medians']:\n");
            sb.append("    x,y = l.get_xydata()[1]\n");
            sb.append("    ax.annotate(str(y), [x,y])\n");

            sb.append(("bp = ax.boxplot(a[1], positions = [2], labels = [labels[1]], showmeans = True)\n"));
            sb.append("for l in bp['medians']:\n");
            sb.append("    x,y = l.get_xydata()[1]\n");
            sb.append("    ax.annotate(str(y), [x,y])\n");

            sb.append(("bp = ax.boxplot(a[2], positions = [3], labels = [labels[2]], showmeans = True)\n"));
            sb.append("for l in bp['medians']:\n");
            sb.append("    x,y = l.get_xydata()[1]\n");
            sb.append("    ax.annotate(str(y), [x,y])\n");

        }

        sb.append("plt.ylabel(\"Number Of Methods\")\n");
        sb.append("fig.savefig(\"python/images/\" + title)\n");

        /*
        sb.append("ax.set_title(title + \" show typefilter\")\n");
        sb.append("ax.boxplot(b, labels=labels, showmeans=True)\n");
        sb.append("fig.savefig(\"python/images/\" + title + \" show typefilter\")\n");
        sb.append("fig, ax = plt.subplots()\n");
        sb.append("ax.set_title(title + \" typefilter not shown\")\n");
        sb.append("ax.boxplot(b[:,1:], labels=labels[1:], showmeans=True)\n");
        sb.append("fig.savefig(\"python/images/\" + title + \" typefilter not shown\")\n");
         */

        program = sb.toString();
    }

    // https://stackoverflow.com/questions/16447410/how-to-execute-python-script-from-java-via-command-line
    // https://www.tutorialkart.com/java/java-write-string-to-file/



}

