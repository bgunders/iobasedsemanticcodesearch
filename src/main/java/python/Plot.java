package python;

import java.io.*;

public abstract class Plot {
    protected String program;

    /*
        Create plot.
         */
    public Object exec(){
        File python = new File("python/");
        File script = new File("python/bp.py");
        File images = new File("python/images/");

        if(!python.exists()){
            images.mkdirs();
        }

        if(!images.exists()){
            images.mkdirs();
        }

        try{
            FileOutputStream fileOutputStream = new FileOutputStream(script);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

            bufferedOutputStream.write(program.getBytes());
            bufferedOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] cmd = {
                "python",
                "python/bp.py"
        };

        Process p = null;
        try {
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
