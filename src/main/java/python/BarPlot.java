package python;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BarPlot extends Plot{

    public BarPlot(Map<Integer, Integer> map, String title, String fileName, String xLabel, String yLabel){
        StringBuilder sb = new StringBuilder();

        List<Integer> x = new ArrayList<>();
        List<Integer> y = new ArrayList<>();

        for(int key : map.keySet()){
            x.add(key);
            y.add(map.get(key));
        }

        sb.append("import matplotlib.pyplot as plt\n");
        sb.append("x = "); sb.append(x);  sb.append("\n");
        sb.append("y = "); sb.append(y); sb.append("\n");
        sb.append("xLabel = \""); sb.append(xLabel); sb.append("\"\n");
        sb.append("yLabel = \""); sb.append(yLabel); sb.append("\"\n");
        sb.append("title = \""); sb.append(title); sb.append("\"\n");

        sb.append("plt.rcParams['figure.dpi'] = 300\n");
        sb.append("plt.rcParams['savefig.dpi'] = 300\n");

        sb.append("fig, ax = plt.subplots()\n");
        sb.append("ax.set_title(title)\n");

        sb.append("ax.bar(x,y, log = True)\n");

        sb.append("plt.xlabel(xLabel)\n");
        sb.append("plt.ylabel(yLabel)\n");

        sb.append("fig.savefig(\"python/images/\" + title)\n");

        program = sb.toString();
    }
}
