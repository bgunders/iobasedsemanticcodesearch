package process.invoke;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public class MethodInvocationCallable<T> implements Callable {
    private boolean done = false;
    private boolean success = false;

    private Object o;
    private Object[] parameters;
    private Method method;

    private Object res;

    private int i;

    public MethodInvocationCallable(Method method, Object o, Object[] parameters, int i){
        this.o = o;
        this.parameters = parameters;
        this.method = method;
        this.i = i;
    }

    public Object call(){
        success = false;
        done = false;

        try {
            if(parameters != null){
                res = method.invoke(o, parameters);
            } else {
                res = null;
            }
            success = true;
        } catch (Exception e) {
        } catch (Error e) {
        }
        done = true;
        return res;
    }

    public boolean isDone(){
        return done;
    }

    public boolean isSuccess(){
        return success;
    }

    public Object getResult(){
        return res;
    }

    public int getI(){return i;}

}