package process.invoke;

import process.Process;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class InvokeProcess extends Process {
    private static Object lock = new Object();

    private static Object[][] params;

    private static boolean sendParams = false;

    public static void main(String[] args) {
        if(args.length >= 1){
            if(args[0].equalsIgnoreCase("param")){
                sendParams = true;
            } else {
                sendParams = false;
            }
        } else {
            sendParams = false;
        }

        try {
            openStreams(58499);

            //System.setOut(null);
            //System.setIn(null);

            params = (Object[][]) objectInputStream.readObject();

            if(params == null){
                closeStreams();
                System.exit(0);
            }

            String jarFilePath = (String) objectInputStream.readObject();
            String className  = (String) objectInputStream.readObject();
            String methodName = (String) objectInputStream.readObject();
            Class[] parameterTypes = (Class[]) objectInputStream.readObject();

            Object[] objs = null;

            if(sendParams){
                objs = (Object[]) objectInputStream.readObject();
            } else {
                objs = new Object[params.length];
            }


            if(objs == null){
                objs = new Object[params.length];
            }

            Method method = loadMethod(jarFilePath, className, methodName, parameterTypes);




            Object[] ret = new Object[params.length];

            if(method == null){
                objectOutputStream.writeObject("method not found");
                return;
            } else {
                objectOutputStream.writeObject("method found");
            }

            Collection<MethodInvocationCallable> callables = new ArrayList<>(params.length);


            for(int i = 0; i < params.length; i++){
                MethodInvocationCallable methodInvocationCallable = new MethodInvocationCallable(method, objs[i], params[i], i);
                callables.add(methodInvocationCallable);
            }

            int numProcessors = Runtime.getRuntime().availableProcessors();

            ScheduleThread[] scheduleThreads = new ScheduleThread[numProcessors];

            Queue<MethodInvocationCallable> queue = new ConcurrentLinkedQueue<>();

            Queue<Integer> done = new ConcurrentLinkedQueue<>();

            Queue<Integer> wontStop = new ConcurrentLinkedQueue<>();


            for(MethodInvocationCallable c : callables){
                queue.add(c);
            }


            for(int i = 0 ; i < scheduleThreads.length; i++){
                scheduleThreads[i] = new ScheduleThread(queue, ret, done,wontStop, objs);
            }

            for(int i = 0; i < scheduleThreads.length; i++){
                scheduleThreads[i].start();
            }

            for(int i = 0; i < scheduleThreads.length; i++){
                try {
                    scheduleThreads[i].join();
                } catch (InterruptedException e) {
                }
            }

            sendStatusMessage(done, null, ret, objs);
        } catch (IOException | ClassNotFoundException e) {
        }
        System.exit(0);
    }

    public static void sendStatusMessage(Queue<Integer> done, Queue<Integer> wontStop, Object[] ret, Object[] objs){
        synchronized (lock){
            try {
                objectOutputStream.writeObject(ret);
                objectOutputStream.writeObject(done);
                objectOutputStream.writeObject(wontStop);

                if(sendParams){
                    objectOutputStream.writeObject(params);
                    objectOutputStream.writeObject(objs);
                }
            } catch (IOException e) {
            }
        }
    }

    public static void sendStatusMessageExit(Queue<Integer> done, Queue<Integer> wontStop, Object[] ret, Object[] objs){
        synchronized (lock){
            try {
                objectOutputStream.writeObject(ret);
                objectOutputStream.writeObject(done);
                objectOutputStream.writeObject(wontStop);

                if(sendParams){
                    objectOutputStream.writeObject(params);
                    objectOutputStream.writeObject(objs);
                }
            } catch (IOException e) {
            }
            closeStreams();
            System.exit(0);
        }
    }

    public static Method loadMethod(String jarFilePath, String className, String methodName, Class[] parameterTypes){
        Method method = null;

        if(!jarFilePath.equals("")){
            File jar = new File(jarFilePath);
            URL[] urls = new URL[1];
            try {
                urls[0] = jar.toURI().toURL();
            } catch (Exception e) {
                e.printStackTrace();
            }

            URLClassLoader uc = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());

            try {
                Class c = Class.forName(className, false, uc);
                method = c.getMethod(methodName, parameterTypes);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
            }
        } else{
            try {
                Class<?> c = Class.forName(className);
                method = c.getMethod(methodName, parameterTypes);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
            }
        }
        return method;
    }
}
