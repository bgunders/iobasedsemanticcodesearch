package process.invoke;

import java.util.Queue;
import java.util.concurrent.*;

public class ScheduleThread extends Thread{

    private Queue<MethodInvocationCallable> queue;
    private Object[] ret;
    private Queue<Integer> done;
    private Queue<Integer> wontStop;
    private Object[] objs;

    public ScheduleThread(Queue<MethodInvocationCallable> queue, Object[] ret, Queue<Integer> done, Queue<Integer> wontStop, Object[] objs){
        this.queue = queue;
        this.ret = ret;
        this.done = done;
        this.wontStop = wontStop;
        this.objs = objs;
    }

    @Override
    public void run(){
        //https://stackoverflow.com/questions/1164301/how-do-i-call-some-blocking-method-with-a-timeout-in-java
        ExecutorService executorService = Executors.newCachedThreadPool();
        int numP = Runtime.getRuntime().availableProcessors();

        MethodInvocationCallable currentCallable = queue.poll();

        while(currentCallable != null){
            Future<Object> future = executorService.submit(currentCallable);
            int i = currentCallable.getI();

            try {
                ret[i] = future.get(1, TimeUnit.SECONDS);
            } catch (Error e) {
            } catch (Exception e) {
            }

            done.add(i);
            if(!future.isDone()){
                wontStop.add(i);

                if(wontStop.size() >= 1){
                    InvokeProcess.sendStatusMessageExit(done, wontStop, ret, objs);
                }
            }

            currentCallable = queue.poll();
        }

        executorService.shutdown();
    }


}

