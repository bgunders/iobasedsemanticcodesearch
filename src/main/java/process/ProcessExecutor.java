package process;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ProcessExecutor {
    protected ServerSocket serverSocket = null;

    protected Socket client = null;

    protected OutputStream outputStream = null;
    protected ObjectOutputStream objectOutputStream = null;

    protected InputStream inputStream = null;
    protected ObjectInputStream objectInputStream = null;

    protected java.lang.Process p = null;

    protected void newServerSocket(int port){
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void newClient(){
        try {
            if(client != null){
                client.close();
            }
            client = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        openSteams();
    }

    protected void openSteams(){
        try {
            outputStream = client.getOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);

            inputStream = client.getInputStream();
            objectInputStream = new ObjectInputStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void startProcess(String exec, int port){
        newServerSocket(port);
        exec(exec);
        newClient();
    }

    protected void restartProcess(String exec){
        destroy();
        exec(exec);
        newClient();
    }

    protected void exec(String exec){
        try {
            p = Runtime.getRuntime().exec(exec);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void destroy(){
        p.destroyForcibly();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void closeAll(){
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (objectInputStream != null) {
                objectInputStream.close();
            }
        } catch (IOException e) {
        }
        if(serverSocket != null){
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(client != null){
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
