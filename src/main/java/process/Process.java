package process;

import java.io.*;
import java.net.Socket;

public class Process {

    protected static InputStream inputStream;
    protected static ObjectInputStream objectInputStream;

    protected static OutputStream outputStream;
    protected static ObjectOutputStream objectOutputStream;

    protected static Socket socket;

    protected static void openStreams(int port) {
        try {
            socket = new Socket("localhost", port);

            inputStream = socket.getInputStream();
            objectInputStream = new ObjectInputStream(inputStream);
            outputStream = socket.getOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);
        } catch (IOException e) { }
    }

    protected static void closeStreams() {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
        }
        try {
            if (objectInputStream != null) {
                objectInputStream.close();
            }
        } catch (IOException e) {
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}
