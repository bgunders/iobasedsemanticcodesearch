package process.compiler;

import compilation.FileCompileCallable;
import process.Process;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.*;

public class CompilerProcess extends Process {
    public static void main(String[] args){
        try {
            openStreams(58494);

            System.setOut(null);
            System.setIn(null);

            ExecutorService executorService = Executors.newSingleThreadExecutor();

            while(true){
                try {
                    File file = (File) objectInputStream.readObject();

                    FileCompileCallable fileCompileCallable = new FileCompileCallable(file);

                    Future future = executorService.submit(fileCompileCallable);

                    try {
                        future.get(10, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                    } catch (ExecutionException e) {
                    } catch (TimeoutException e) {

                    }

                    if(!future.isDone()){
                        objectOutputStream.writeObject(1);
                        closeStreams();
                        System.exit(0);
                    }
                    objectOutputStream.writeObject(0);

                } catch (ClassNotFoundException e) { }
            }
        } catch (IOException e) { }

        closeStreams();
        System.exit(0);
    }
}
