package utils;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class HashUtilsTest {

    void assertCustom(Object a, Object b){
        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    void testToHashable() {
        short[] toTest = {1,2,3,4,5};

        List<Short> r = new ArrayList<>();
        r.add((short) 1);
        r.add((short) 2);
        r.add((short) 3);
        r.add((short) 4);
        r.add((short) 5);

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable1() {
        int[][] toTest = {{1,2},{3,4}};

        List<List<Integer>> r = new ArrayList<>();
        List<Integer> r1 = new ArrayList<>();
        r1.add(1);
        r1.add(2);
        r.add(r1);
        List<Integer> r2 = new ArrayList<>();
        r2.add(3);
        r2.add(4);
        r.add(r2);

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable2() {
        byte[][][] toTest = {{{0,0,0}}};

        List<List<List<Byte>>> r = new ArrayList<>();
        List<List<Byte>> r2 = new ArrayList<>();
        r.add(r2);
        List<Byte> r3 = new ArrayList<>();
        r2.add(r3);
        r3.add((byte) 0);
        r3.add((byte) 0);
        r3.add((byte) 0);

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable3() {
        Boolean[][] toTest = {{true}, {false}};

        List<List<Boolean>> r = new ArrayList<>();
        List<Boolean> r2 = new ArrayList<>();
        r2.add(true);
        r.add(r2);
        List<Boolean> r3 = new ArrayList<>();
        r.add(r3);
        r3.add(false);




        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable4() {
        boolean[][] toTest = {{false}, {true}};

        List<List<Boolean>> r = new ArrayList<>();
        List<Boolean> r2 = new ArrayList<>();
        r.add(r2);
        r2.add(false);
        List<Boolean> r3 = new ArrayList<>();
        r.add(r3);
        r3.add(true);

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable5() {
        Integer[][] toTest = {{1,2},{3,4}};

        List<List<Integer>> r = new ArrayList<>();
        List<Integer> r1 = new ArrayList<>();
        r1.add(1);
        r1.add(2);
        r.add(r1);
        List<Integer> r2 = new ArrayList<>();
        r2.add(3);
        r2.add(4);
        r.add(r2);
        Object ret = HashUtils.toHashable(toTest);

        assertCustom(r, ret);
    }

    @Test
    void testToHashable6() {
        List<List<Short>> s = new ArrayList<>();
        List<Short> s1 = new ArrayList<>();
        s1.add((short) 1);
        s1.add((short) 2);
        s.add(s1);
        List<Short> s2 = new ArrayList<>();
        s2.add((short) 3);
        s2.add((short) 4);
        s.add(s2);

        List<List<Integer>> integers = new ArrayList<>();
        List<Integer> i1 = new ArrayList<>();
        i1.add(1);
        i1.add(2);
        integers.add(i1);
        List<Integer> i2 = new ArrayList<>();
        i2.add(3);
        i2.add(4);
        integers.add(i2);

        assertEquals(s.hashCode(), integers.hashCode());
    }

    @Test
    void testToHashable7() {
        Double[][] toTest = {{1.0,2.0}};

        List<List<Double>> d = new ArrayList<>();
        List<Double> d2 = new ArrayList<>();
        d.add(d2);
        d2.add(1.0);
        d2.add(2.0);

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(d, ret);

    }

    @Test
    void testToHashable8(){
        List<List<Integer>> integers = new ArrayList<>();
        List<Integer> i1 = new ArrayList<>();
        i1.add(1);
        i1.add(2);
        integers.add(i1);
        List<Integer> i2 = new ArrayList<>();
        i2.add(3);
        i2.add(4);
        integers.add(i2);

        List<List<Long>> longs = new ArrayList<>();
        List<Long> l1 = new ArrayList<>();
        l1.add(1L);
        l1.add(2L);
        longs.add(l1);
        List<Long> l2 = new ArrayList<>();
        l2.add(3L);
        l2.add(4L);
        longs.add(l2);

        assertEquals(integers.hashCode(), longs.hashCode());
    }

    @Test
    void testToHashable9(){
        Integer a = 1;

        Object ret = HashUtils.toHashable(a);

        assertCustom(a, ret);
    }

    @Test
    void testToHashable10(){
        List<Integer> a = new ArrayList<>();
        a.add(1);

        Object ret = HashUtils.toHashable(a);

        assertCustom(a, ret);
    }

    @Test
    void testToHashable11(){
        List<Class> a = new ArrayList<>();
        a.add(int.class);
        a.add(int.class);
        a.add(boolean.class);

        Class[] toTest = {int.class, int.class, boolean.class};

        Object ret = HashUtils.toHashable(toTest);

        assertCustom(a, ret);
    }

    @Test
    void testToHashable12(){
        Integer[][][] toTest = {{{1,2}}};

        List<List<List<Integer>>> i = new ArrayList<>();
        List<List<Integer>> i2 = new ArrayList<>();
        i.add(i2);
        List<Integer> i3 = new ArrayList<>();
        i2.add(i3);
        i3.add(1);
        i3.add(2);

        Object ret = HashUtils.toHashable(toTest);


        assertCustom(ret, i);
    }

    @Test
    void testToHashable13(){
        int a = 1;

        Object ret = HashUtils.toHashable(a);
        System.out.println(ret.getClass());
    }
}