package inputoutputMatching;

import methods.MethodContainer;
import org.junit.jupiter.api.Test;
import python.BarPlot;
import python.BoxPlot;
import utils.HashUtils;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class InputOutputMatcherTest {

    public InputOutputMatcherTest(){

    }

    @Test
    void getInputOutputTypeTest() {
        Class[] in = {int.class, boolean.class};
        Class out = long.class;

        List<Class> target = new ArrayList<>();
        target.add(int.class);
        target.add(boolean.class);
        target.add(long.class);

        List<Class> ret = InputOutputMatcher.getInputOutputType(in, out);

        assertEquals(ret.hashCode(), target.hashCode());
        assertEquals(ret, target);

    }

    Set<Set<Object>> powerSet(List<Object> vals){
        Set<Set<Object>> powerSet = new HashSet<>();
        Set<Object> empty = new HashSet<>();
        powerSet.add(empty);
        return powerSet(vals, powerSet);
    }

    Set<Set<Object>> powerSet(List<Object> vals, Set<Set<Object>> prevSet){
        Set<Set<Object>> powerSet = new HashSet<>(prevSet);

        for(int i = 0; i < vals.size(); i++){
            Object val = vals.get(i);

            for(Set<Object> list : prevSet){
                List<Object> valsCopy = new ArrayList<>(vals);
                valsCopy.remove(i);

                Set<Set<Object>> setCopy = new HashSet<>(prevSet);

                Set<Object> listCopy = new HashSet<>(list);
                listCopy.add(val);
                powerSet.add(listCopy);

                setCopy.add(listCopy);

                if(valsCopy.size() > 0){
                    powerSet.addAll(powerSet(valsCopy, setCopy));
                }

            }
        }
        return powerSet;
    }


    /*

     */
    @Test
    void randomIO(){
        int[] temps = {0,1,2,3};

        List<Object> tempsList = (List<Object>) HashUtils.toHashable(temps);
        Set<Set<Object>> allTemplateSets = powerSet(tempsList);

        InputOutputMatcher inputOutputMatcher = InputOutputMatcher.deserialize();
        System.out.println("Deserialized");

        Map<Integer, MethodContainer> methodMap = inputOutputMatcher.methodMap;

        Set<Integer> integerSet = methodMap.keySet();

        Map<List<Object>, Set<Integer>> noClassMap = createClassMap(inputOutputMatcher);

        int numIterations = 5;
        int numRep = 10;

        Collection<MethodContainer> methods = inputOutputMatcher.methodMap.values();

        Map<List<Class>, Set<MethodContainer>> paramMap = new HashMap<>();
        Map<List<Class>, Integer> paramCount = new HashMap<>();
        Map<Integer, Integer> numNonPrimitives = new HashMap<>();

        List<Class> ioType;
        for(MethodContainer method : methods){
            ioType = (List<Class>) HashUtils.toHashable(method.getParameterTypes());

            if(!paramMap.containsKey(ioType)){
                paramMap.put(ioType, new HashSet<>());
            }

            List<Class> paramList = (List<Class>) HashUtils.toHashable(method.getParameterTypes());

            if(!paramCount.containsKey(paramList)){
                paramCount.put(paramList, 0);
            }

            paramCount.put(paramList, paramCount.get(paramList) + 1);

            paramMap.get(ioType).add(method);

            int nonPrimitive = 0;
            for(Class parameterType : method.getParameterTypes()){
                if(!parameterType.isPrimitive()){
                    nonPrimitive++;
                }
            }

            if(!numNonPrimitives.containsKey(nonPrimitive)){
                numNonPrimitives.put(nonPrimitive, 0);
            }

            numNonPrimitives.put(nonPrimitive, numNonPrimitives.get(nonPrimitive) + 1);
        }

        int total = 0;

        Map<Integer, Integer> typeCount = new HashMap<>();


        for(List<Class> key : paramMap.keySet()){
            if(!typeCount.containsKey(key.size())){
                typeCount.put(key.size(), 0);
            }

            typeCount.put(key.size(), typeCount.get(key.size()) + paramMap.get(key).size());
        }
        System.out.println("nonPrimitives");
        System.out.println(numNonPrimitives);

        System.out.println("typeCount");
        System.out.println(typeCount);

        Map<Integer, Integer> sameTypeToTotal = new HashMap<>();

        for(List<Class> key : paramCount.keySet()){
            int num = paramCount.get(key);

            if(!sameTypeToTotal.containsKey(num)){
                sameTypeToTotal.put(num, 0);
            }

            sameTypeToTotal.put(num, sameTypeToTotal.get(num) + 1);
        }

        System.out.println("test");
        System.out.println(sameTypeToTotal);

        BarPlot barPlot;
        barPlot = new BarPlot(numNonPrimitives, "Non Primitives", "Non Primitives", "Number Of Non Primitives", "Number Of Methods");
        barPlot.exec();
        barPlot = new BarPlot(typeCount, "Parameter Count", "Parameter Count", "Number Of Parameters", "Number Of Methods");
        barPlot.exec();
        barPlot = new BarPlot(sameTypeToTotal, "Different Parameter Sizes", "Different Parameter Sizes", "Number Of Methods With Same Parameter Signature", "Number of Parameter Signatures");
        barPlot.exec();


        int[] in = {4,-1,4,2};
        int[] out = {-1,2,4,4};

        Class[] inT = {int[].class};
        Class outT = int[].class;

        Object[] inA = {in};


        Set<Integer> res = inputOutputMatcher.getIndexes(inT, outT, inA , out);
        Set<MethodContainer> methodContainerSet = inputOutputMatcher.getMethods(res);


        Function<MethodContainer, Boolean> allMethods = (MethodContainer methodContainer) -> true;
        Function<MethodContainer, Boolean> allButBooleans = (MethodContainer methodContainer) ->
                !(methodContainer.getReturnType() == boolean.class || methodContainer.getReturnType() == Boolean.class);
        Function<MethodContainer, Boolean> onlyBooleans = (MethodContainer methodContainer) ->
                methodContainer.getReturnType() == boolean.class || methodContainer.getReturnType() == Boolean.class;

        Function<MethodContainer, Set<List<Object>>> allIoSet = MethodContainer::getInputOutputSet;

        Function<Set<Object>, Function<MethodContainer, Set<List<Object>>>> ioSetCreator = (Set<Object> templates) -> (MethodContainer methodContainer) -> {
            Set<List<Object>> inputOutputSet = new HashSet<>();
            Set<List<Object>> all = methodContainer.getInputOutputSet();

            Map<List<Object>, Integer> ioToTemplate = methodContainer.getIoToTemplate();

            for (List<Object> io : all) {
                if (templates.contains(ioToTemplate.get(io))) {
                    inputOutputSet.add(io);
                }
            }
            return inputOutputSet;
        };

        BiFunction<MethodContainer, List<Object>, Set<Integer>> setOfNonClassMap = (MethodContainer methodContainer, List<Object> io) -> noClassMap.get(io);
        BiFunction<MethodContainer, List<Object>, Set<Integer>> setOfTypeMap = (MethodContainer methodContainer, List<Object> io) -> inputOutputMatcher.inputOutputMap.get(methodContainer.getIoToType().get(io)).get(io);

        Function<Set<Integer>, Map<Set<List<Object>>, Set<Integer>>> groupDuplicates = (Set<Integer> set) -> {
            Map<Set<List<Object>>, Set<Integer>> ioPairsSet = new HashMap<>();

            for(int i : set){
                MethodContainer methodContainer = inputOutputMatcher.methodMap.get(i);
                Set<List<Object>> inputOutputSet = methodContainer.getInputOutputSet();
                List<Class> type = InputOutputMatcher.getInputOutputType(methodContainer.getParameterTypes(), methodContainer.getReturnType());

                List<Object> typeO = new ArrayList<>();

                for(Class c : type){
                    typeO.add(c);
                }

                Set<List<Object>> inputOutputSetCopy = new HashSet<>(inputOutputSet);

                inputOutputSetCopy.add(typeO);

                if(!ioPairsSet.containsKey(inputOutputSetCopy)){
                    ioPairsSet.put(inputOutputSetCopy, new HashSet<>());
                }

                ioPairsSet.get(inputOutputSetCopy).add(i);
            }

            return ioPairsSet;
        };

        Map<Set<List<Object>>, Set<Integer>> grouped = groupDuplicates.apply(integerSet);

        System.out.println(grouped.keySet().size());

        for(Set<List<Object>> set : grouped.keySet()){
            if(grouped.get(set).size() > 40){
                System.out.println(grouped.get(set).size());
                for(MethodContainer methodContainer : inputOutputMatcher.getMethods(grouped.get(set))){
                    System.out.println(methodContainer);
                    System.out.println(methodContainer.getInputOutputSet());
                    break;
                }
            }
        }

        Map<Integer, Integer> map = new HashMap<>();

        for(Set<List<Object>> key : grouped.keySet()){
            Set<Integer> set = grouped.get(key);

            int size = set.size();

            if(!map.containsKey(size)){
                map.put(size, 0);
            }

            map.put(size, map.get(size) + 1);
        }

        BarPlot barPlotGrouped = new BarPlot(map, "Grouped", "Grouped", "Number Of Methods In Group", "Number Of Groups Of That Size");
        barPlotGrouped.exec();

        evaluateType("Types Size", inputOutputMatcher, allMethods, onlyBooleans, allButBooleans, groupDuplicates);
        System.exit(0);

        evaluate("All Methods; Type Access", inputOutputMatcher, numIterations, numRep, allMethods, allIoSet, setOfTypeMap, groupDuplicates);
        evaluate("Boolean Return Methods; Type Access", inputOutputMatcher, numIterations, numRep, onlyBooleans, allIoSet, setOfTypeMap, groupDuplicates);
        evaluate("Non Boolean Return Methods; Type Access", inputOutputMatcher, numIterations, numRep, allButBooleans, allIoSet, setOfTypeMap, groupDuplicates);

        evaluate("All Methods; Direct Access", inputOutputMatcher, numIterations, numRep, allMethods, allIoSet, setOfNonClassMap, groupDuplicates);
        evaluate("Boolean Return Methods; Direct Access", inputOutputMatcher, numIterations, numRep, onlyBooleans, allIoSet, setOfNonClassMap, groupDuplicates);
        evaluate("Non Boolean Return Methods; Direct Access", inputOutputMatcher, numIterations, numRep, allButBooleans, allIoSet, setOfNonClassMap, groupDuplicates);

        for(Set<Object> templateSet : allTemplateSets){
            if(templateSet.size() == 0){
                continue;
            }

            System.out.println(templateSet);
            Function<MethodContainer, Set<List<Object>>> ioSet = ioSetCreator.apply(templateSet);

            evaluate("Type " + templateSet.toString(), inputOutputMatcher, numIterations, numRep, allMethods, ioSet, setOfTypeMap, groupDuplicates);
            evaluate("Direct " + templateSet.toString(), inputOutputMatcher, numIterations, numRep, allMethods, ioSet, setOfNonClassMap, groupDuplicates);
        }
    }

    private void evaluateType(String title, InputOutputMatcher inputOutputMatcher,
                              Function<MethodContainer, Boolean> all,
                              Function<MethodContainer, Boolean> bool,
                              Function<MethodContainer, Boolean> allButBool,
                              Function<Set<Integer>, Map<Set<List<Object>>, Set<Integer>>> groupDuplicates) {
        Map<Integer, MethodContainer> methodMap = inputOutputMatcher.methodMap;

        int total = 0;

        Set<Integer> integerSet = methodMap.keySet();




        double[][] methodSetsize = new double[3][integerSet.stream().max(Integer::compare).get() + 1];
        double[][] methodSetsizeGrouped = new double[3][integerSet.stream().max(Integer::compare).get() + 1];


        for(int i : integerSet){
            MethodContainer methodContainer = methodMap.get(i);

            Map<List<Class>, Set<Integer>> typeMap = inputOutputMatcher.typeMap;
            Class[] parameterTypes = methodContainer.getParameterTypes();
            Set<Class> outputClasses = methodContainer.getOutputClasses();

            Set<Integer> methods = new HashSet<>();
            //System.out.println(methodContainer);

            for(Class outputType : outputClasses){
                List<Class> typeIndex = InputOutputMatcher.getInputOutputType(parameterTypes, outputType);
                Set<Integer> typeMethods = typeMap.get(typeIndex);

                if(typeMethods == null){
                    System.out.println(typeIndex);
                    continue;
                }
                methods.addAll(typeMethods);
            }

            int groupedSize = groupDuplicates.apply(methods).keySet().size();
            int size = methods.size();

            if (all.apply(methodContainer)) {
                methodSetsize[0][i] = size;
                methodSetsizeGrouped[0][i] = groupedSize;
            }

            if(bool.apply(methodContainer)){
                methodSetsize[1][i] = size;
                methodSetsizeGrouped[1][i] = groupedSize;
            }

            if(allButBool.apply(methodContainer)){
                methodSetsize[2][i] = size;
                methodSetsizeGrouped[2][i] = groupedSize;
            }
        }

        double[][] temp = new double[3][];
        double[][] tempGrouped = new double[3][];

        for(int i = 0; i < 3; i++){
            int count = 0;

            for(int j = 0; j < methodSetsize[i].length; j++){
                if(methodSetsize[i][j] > 0){
                    count++;
                }
            }

            temp[i] = new double[count];
            tempGrouped[i] = new double[count];

            int k = 0;

            for(int j = 0; j < methodSetsize[i].length; j++){
                if(methodSetsize[i][j] > 0){
                    temp[i][k] = methodSetsize[i][j];
                    tempGrouped[i][k] = methodSetsizeGrouped[i][j];
                    k++;
                }
            }


        }

        methodSetsize = temp;
        methodSetsizeGrouped = tempGrouped;

        System.out.println("total = " + total);

        BoxPlot boxPlot = new BoxPlot(Arrays.deepToString(methodSetsize), 3, title, 2);
        boxPlot.exec();
        BoxPlot boxPlotGrouped = new BoxPlot(Arrays.deepToString(methodSetsizeGrouped), 3, "Grouped; " + title, 2);
        boxPlotGrouped.exec();
        System.out.println("done");
    }

    private void evaluate(String title, InputOutputMatcher inputOutputMatcher, int numIterations, int numRep,
                          Function<MethodContainer, Boolean> methodSelector,
                          Function<MethodContainer, Set<List<Object>>> ioSetFunction,
                          BiFunction<MethodContainer, List<Object>, Set<Integer>> ioToSet,
                          Function<Set<Integer>, Map<Set<List<Object>>, Set<Integer>>> groupDuplicates
                          ) {
        Map<Integer, MethodContainer> methodMap = inputOutputMatcher.methodMap;

        int total = 0;

        Set<Integer> integerSet = methodMap.keySet();

        double[][] methodSetsize = new double[integerSet.stream().max(Integer::compare).get() + 1][numIterations];
        double[][] methodSetsizeGroup = new double[integerSet.stream().max(Integer::compare).get() + 1][numIterations];

        for(int rep = 0; rep < numRep; rep++){
            for(int i : integerSet){

                MethodContainer methodContainer = methodMap.get(i);

                if(!methodSelector.apply(methodContainer)){
                    continue;
                }

                Set<List<Object>> inputOutputSet = ioSetFunction.apply(methodContainer);
                if(inputOutputSet.size() == 0){
                    total++;
                    continue;
                }

                Set<Integer> matchedMethods = new HashSet<>(integerSet);

                for(int iteration = 0; iteration < numIterations; iteration++){
                    List<Object> inputOutput = getRandomElement(inputOutputSet);

                    Set<Integer> possibleMethods = ioToSet.apply(methodContainer, inputOutput);

                    matchedMethods.retainAll(possibleMethods);

                    Map<Set<List<Object>>, Set<Integer>> map = groupDuplicates.apply(matchedMethods);

                    methodSetsize[i][iteration] += matchedMethods.size();
                    methodSetsizeGroup[i][iteration] += map.keySet().size();

                }


                assertTrue(matchedMethods.contains(i));
            }
        }

        for (int i = 0; i < methodSetsize.length; i++){
            for(int j = 0; j < methodSetsize[i].length; j++){
                methodSetsize[i][j] /= numRep;
            }
        }

        for (int i = 0; i < methodSetsizeGroup.length; i++){
            for(int j = 0; j < methodSetsizeGroup[i].length; j++){
                methodSetsizeGroup[i][j] /= numRep;
            }
        }

        System.out.println("total = " + total / numRep);

        double[][] removed = removeZeroArrays(methodSetsize);
        double[][] removedGroup = removeZeroArrays(methodSetsizeGroup);
        BoxPlot boxPlot = new BoxPlot(Arrays.deepToString(removed), numIterations, title, 1);
        boxPlot.exec();
        BoxPlot boxPlotGroup = new BoxPlot(Arrays.deepToString(removedGroup), numIterations, "Grouped; " + title, 1);
        boxPlotGroup.exec();
        System.out.println("done");
    }

    private Map<List<Object>, Set<Integer>> createClassMap(InputOutputMatcher inputOutputMatcher) {
        Map<List<Object>, Set<Integer>> noClassMap = new HashMap<>();

        Map<List<Class>, Map<List<Object>, Set<Integer>>> inputOutputMap = inputOutputMatcher.inputOutputMap;

        for(List<Class> key1 : inputOutputMap.keySet()){
            Map<List<Object>, Set<Integer>> ioMap = inputOutputMap.get(key1);

            for(List<Object> key2 : ioMap.keySet()){


                Set<Integer> ints = ioMap.get(key2);

                if (!noClassMap.containsKey(key2)){
                    noClassMap.put(key2, new HashSet<>());
                }
                noClassMap.get(key2).addAll(ints);

            }
        }

        return noClassMap;
    }

    /*
    Test the difference between first applying type filtering versus not applying it.
    time1 measures using type filtering
    time2 measures without using type filter
    time3 asserts that results without using a type filter result in a subset of results without type filter.
    The set of methods without type filter can be larger since double[] and Double[] will both be converted to
    List<Double> in order to be hashed.

    result: 10 tries, 100000 invokes.
    time1 average = OptionalDouble[1.171429378E9]; [1515352000, 1281588411, 1139857045, 1120674719, 1134133235, 1116020354, 1094939956, 1113574518, 1093662255, 1104491287]
    time2 average = OptionalDouble[1.1774429245E9]; [1187405844, 1300774567, 1140807757, 1170815077, 1167975440, 1159098917, 1175928051, 1155739168, 1152737113, 1163147311]
    time2 average = OptionalDouble[1.2510927785E9]; [1349148002, 1347617717, 1234923577, 1199547006, 1223143736, 1213212774, 1258044342, 1229363127, 1229217866, 1226709638]

    Interpretation: We can use type filter to get more accurate results with no noticeable performance difference in this set of methods.
     */
    @Test
    void classVsNoClassMapTest(){
        InputOutputMatcher inputOutputMatcher = InputOutputMatcher.deserialize();
        System.out.println("Deserialized");

        Map<List<Class>, Map<List<Object>, Set<Integer>>> inputOutputMap = inputOutputMatcher.inputOutputMap;

        Map<List<Object>, Set<Integer>> noClassMap = new HashMap<>();


        for(List<Class> key1 : inputOutputMap.keySet()){
            Map<List<Object>, Set<Integer>> ioMap = inputOutputMap.get(key1);

            for(List<Object> key2 : ioMap.keySet()){
                Set<Integer> ints = ioMap.get(key2);

                if (!noClassMap.containsKey(key2)){
                    noClassMap.put(key2, new HashSet<>());
                }
                noClassMap.get(key2).addAll(ints);

            }
        }

        int numInvoke = 100000;

        Runnable test1 = () -> {
            for (int i = 0; i < numInvoke; i++) {
                List<Class> classIndex = getRandomElement(inputOutputMap.keySet());
                List<Object> objectIndex = getRandomElement(inputOutputMap.get(classIndex).keySet());

                Set<Integer> ret = inputOutputMap.get(classIndex).get(objectIndex);
            }
        };


        Runnable test2 = () -> {
            for (int i = 0; i < numInvoke; i++) {

                List<Class> classIndex = getRandomElement(inputOutputMap.keySet());
                List<Object> objectIndex = getRandomElement(inputOutputMap.get(classIndex).keySet());

                Set<Integer> ret = noClassMap.get(objectIndex);
            }
        };

        Runnable test3 = () -> {
            for (int i = 0; i < numInvoke; i++) {

                List<Class> classIndex = getRandomElement(inputOutputMap.keySet());
                List<Object> objectIndex = getRandomElement(inputOutputMap.get(classIndex).keySet());

                Set<Integer> ret1 = inputOutputMap.get(classIndex).get(objectIndex);
                Set<Integer> ret2 = noClassMap.get(objectIndex);

                assertTrue(ret2.containsAll(ret1));
            }
        };

        int numTries = 10;

        long[] time1 = new long[numTries];
        long[] time2 = new long[numTries];
        long[] time3 = new long[numTries];

        for(int i = 0; i < numTries; i++){
            time1[i] = time(test1);
            time2[i] = time(test2);
            time3[i] = time(test3);
        }


        System.out.println("time1 average (ns) = " + Arrays.stream(time1).average() + "; " + Arrays.toString(time1));
        System.out.println("time2 average (ns) = " + Arrays.stream(time2).average() + "; " + Arrays.toString(time2));
        System.out.println("time2 average (ns) = " + Arrays.stream(time3).average() + "; " + Arrays.toString(time3));

        System.out.println("time1 / numInvoke average (ns) = " + (Arrays.stream(time1).average().getAsDouble() / ((float)numInvoke)) + "; " + Arrays.toString(time1));
        System.out.println("time2 / numInvoke average (ns) = " + (Arrays.stream(time2).average().getAsDouble() / ((float)numInvoke)) + "; " + Arrays.toString(time2));
        System.out.println("time2 / numInvoke average (ns) = " + (Arrays.stream(time3).average().getAsDouble() / ((float)numInvoke)) + "; " + Arrays.toString(time3));
    }

    private double[][] removeZeroArrays(double[][] array){
        ArrayList<double[]> nonZs = new ArrayList<>();

        for(double[] m : array){
            boolean allZero = true;

            for(double i : m){
                if(i != 0){
                    allZero = false;
                    break;
                }
            }

            if(!allZero){
                nonZs.add(m);
                if(m == null){
                    System.out.println("ye");
                }
            }
        }

        double[][] arr;

        if(nonZs.size() > 0){
            arr  = new double[nonZs.size()][nonZs.get(0).length];
        } else {
            arr = new double[0][0];
        }

        for(int i = 0; i < nonZs.size(); i++){
            arr[i] = nonZs.get(i);
            if(arr[i] == null){
                System.out.println("hmm");
            }
        }

        return arr;
    }

    private static <T> T getRandomElement(Set<T> set){
        return getRandomElement(new ArrayList<>(set));
    }

    private static <T> T getRandomElement(List<T> list){
        int size = list.size();
        int random = (int) (Math.random() * size);
        return list.get(random);
    }

    private static long time(Runnable r){
        long start = System.nanoTime();
        r.run();
        long end = System.nanoTime();

        return end - start;
    }



}