# ioBasedSemanticCodeSearch
Search for methods given &lt;input, output> pairs. Written in and for Java.

## How to compile
Download the prototype using git:
git clone git@gitlab.ethz.ch:bgunders/iobasedsemanticcodesearch.git

To compile the prototype maven is used. 
The prototype uses java 1.8.
Python and matplotlib (pip install matplotlib) are required to create the plots.

To compile the project you can use maven in the following way:
mvn package

This will create three jars in the target/ directory:target/prototype.ja}, target/InvokeProcess.jar and target/CompilerProcess.jar

Start the prototype by typting:
java -jar target/prototype.jar

Enter h for help.

Note that the prototype was written, compiled and tested on manjaro linux.

## Java Source Files
Repositories to test this can be found here:
[GitHub Java Corpus](https://groups.inf.ed.ac.uk/cup/javaGithub/)
