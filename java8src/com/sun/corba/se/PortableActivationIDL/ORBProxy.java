package com.sun.corba.se.PortableActivationIDL;


/**
* com/sun/corba/se/PortableActivationIDL/ORBProxy.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/java8-openjdk/src/jdk8u-jdk8u282-b08/corba/src/share/classes/com/sun/corba/se/PortableActivationIDL/activation.idl
* Tuesday, February 9, 2021 11:50:16 PM UTC
*/


/** ORB callback interface, passed to Activator in registerORB method.
    */
public interface ORBProxy extends ORBProxyOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity 
{
} // interface ORBProxy
