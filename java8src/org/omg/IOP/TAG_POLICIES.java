package org.omg.IOP;


/**
* org/omg/IOP/TAG_POLICIES.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/java8-openjdk/src/jdk8u-jdk8u282-b08/corba/src/share/classes/org/omg/PortableInterceptor/IOP.idl
* Tuesday, February 9, 2021 11:50:16 PM UTC
*/

public interface TAG_POLICIES
{

  /**
       * A profile component containing the sequence of QoS policies exported
       * with the object reference by an object adapter.
       */
  public static final int value = (int)(2L);
}
