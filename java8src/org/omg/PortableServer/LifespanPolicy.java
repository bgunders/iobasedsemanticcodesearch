package org.omg.PortableServer;


/**
* org/omg/PortableServer/LifespanPolicy.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from /build/java8-openjdk/src/jdk8u-jdk8u282-b08/corba/src/share/classes/org/omg/PortableServer/poa.idl
* Tuesday, February 9, 2021 11:50:16 PM UTC
*/


/**
	 * The LifespanPolicy specifies the lifespan of the 
	 * objects implemented in the created POA. The default 
	 * is TRANSIENT.
	 */
public interface LifespanPolicy extends LifespanPolicyOperations, org.omg.CORBA.Policy, org.omg.CORBA.portable.IDLEntity 
{
} // interface LifespanPolicy
